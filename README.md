###  ptovtu

This program reads the outputs of a f.e. analysis (in "p-file" format) and writes them in vtu format.

It uses the VTKFortran library of S. Zaghi (https://github.com/szaghi/VTKFortran.git)

### Features

Ascii and binary p-file formats are supported (see more about this format in the doc/ subdirectory) as well as the old (and obsolete) adeli p-file format.

When launched for the very first time, ptovu creates on your home directory a file named .ptovtuUserDefault. This file contains a small catalog of physical quantities with their possible units. You can modify this file to define other physical quantities and to choose the units that best suit your applications. 

In addition, you can specify in this file some secondary quantities that ptovtu will calculate (predefined invariants of rank-2 tensors, eigenvalues and eigenvectors).

### Installation

Clone the project with the --recurse-submodules option:

`git  clone  --recurse-submodules  https://gitlab.oca.eu/hassani/ptovtu.git`


### Compilation

Type 'make comp=your_compiler' (e.g. make comp=ifort) in the main directory or simply 'make' to get help.

Tested on :
- mac OS with ifort (2021.6.0), gfortran (GCC 11.3.0) and nagfor (7.1(Hanzomon))
- on linux with gfortran (GCC 11.2.1 (Red Hat 11.2.1-9))

### Help

To get help run ptovu with the -h option.

### Documentation

See the subdirectory doc/ 

