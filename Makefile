export dirvtkfortran = $(PWD)/third_party/VtkFortran
export dirpk2        = $(PWD)/third_party/pk2
export dircalmat     = $(PWD)/third_party/pk2/app/calmat

define colorecho
      @tput setaf 1
      @echo $1
      @tput sgr0
endef
#
 
help:
#h |--------------------------------------------------------------------------|
#h | Usage:  make comp = <yourCompiler>  [opt = "<yourOptions>"] [clean]      |                          
#h |                                                                          |
#h |                                                                          |
#h | where                                                                    |
#h |   . <yourCompiler>: is the name of your compiler (note: this version was |
#h |                     tested with ifort, gfortran and nagfor).             |
#h |   . <yourOptions> : is a string corresponding to a set of compilation    |
#h |                     options (default is opt="-O3").                      |
#h |                                                                          |
#h | Examples:                                                                |
#h |   . make                                                                 |
#h |        prints this help.                                                 |
#h |   . make comp=ifort clean                                                |
#h |        deletes the object files (from the directory obj/ifort),          |
#h |                the module files (from the directory mod/ifort).          |
#h |   . make comp=gfortran                                                   |
#h |        compiles with -O3 optimization flag.                              |
#h |   . make comp=ifort opt="-O3 -fp-model fast=2 -parallel"                 |
#h |         compiles with the specified options.                             |
#h |   . make comp=ifort opt=check                                            |
#h |         compiles with checking options (predefined according to one of   |
#h |         the three compilers cited above).                                |
#h |--------------------------------------------------------------------------|
ifeq ($(comp),)
   help:     #// Show this help.
	@sed -ne '/@sed/!s/#h //p' $(MAKEFILE_LIST)
else

	@echo " "
	$(call colorecho, "==> Compiling the pk2 library:")
	@echo " "	
	@( cd third_party/pk2; $(MAKE) )

	@echo " "
	$(call colorecho, "==> Compiling the VTKFortran library:")
	@echo " "	
	@( cd third_party; $(MAKE) -f makefiles/makefile_vtk )

	@echo " "
	$(call colorecho, "==> Compiling ptovtu:")
	@echo " "
	@( cd src; $(MAKE) -f makefile_ptovtu )
	 
clean:
	@echo " "
	$(call colorecho,"==> Cleaning ptovtu:")
	@echo " "
	rm -f ./obj/$(comp)/*.o 
	rm -f ./mod/$(comp)/*.mod 

cleanpk2:
	@echo " "
	$(call colorecho,"==> Cleaning pk2 and calmat:")
	@echo " "
	rm -f $(dirpk2)/obj/$(comp)/*.o 
	rm -f $(dirpk2)/mod/$(comp)/*.mod 
	rm -f $(dirpk2)/lib/$(comp)/*.a 
	rm -f $(dircalmat)/obj/$(comp)/*.o 
	rm -f $(dircalmat)/mod/$(comp)/*.mod 
	rm -f $(dircalmat)/lib/$(comp)/*.a 

cleanvtk:
	@echo " "
	$(call colorecho,"==> Cleaning VTKFortran:")
	@echo " "
	rm -f $(dirvtkfortran)/obj/$(comp)/*.o 
	rm -f $(dirvtkfortran)/mod/$(comp)/*.mod 

cleanall:
	@echo " "
	$(call colorecho,"==> Cleaning all:")
	@echo " "
	rm -f $(dirvtkfortran)/obj/$(comp)/*.o 
	rm -f $(dirvtkfortran)/mod/$(comp)/*.mod 
	rm -f $(dirpk2)/obj/$(comp)/*.o 
	rm -f $(dirpk2)/mod/$(comp)/*.mod 
	rm -f $(dirpk2)/lib/$(comp)/*.a 
	rm -f $(dircalmat)/obj/$(comp)/*.o 
	rm -f $(dircalmat)/mod/$(comp)/*.mod 
	rm -f $(dircalmat)/lib/$(comp)/*.a 
	rm -f ./obj/$(comp)/*.o 
	rm -f ./mod/$(comp)/*.o 
endif
