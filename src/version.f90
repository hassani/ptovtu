!---------------------------------------------------------------------------------------------
! ptovtu, version 2022.7.0
!---------------------------------------------------------------------------------------------
!
! Module name: 
!       version_m
!
! Description: 
!       All that depends on the version
! 
! Notes: 
!
! Author(s): 
!        R. Hassani, Universite Côte d'Azur
!
! Start date: 
!        08/22
!
! Changes:
!        
!---------------------------------------------------------------------------------------------

MODULE version_m
         
   implicit none
   
   private
   public :: version_getVersion
   
   character(len=*), parameter :: VERSIONID = '2022.7.0'      ! release(year.month).version
   character(len=*), parameter :: FDEF = '.ptovtuUserDefault' ! user default file

CONTAINS

!=============================================================================================
   SUBROUTINE version_getVersion 
!=============================================================================================
   use globalParameters_m
!---------------------------------------------------------------------------------------------

   numversion = VERSIONID
   filedef    = FDEF
   
#ifdef __CDATE
   compilDate = __CDATE
#else
   compilDate = ""
#endif   

#ifdef __COPTS   
   compilOPts = __COPTS
#else
   compilOPts = ""
#endif     

   END SUBROUTINE version_getVersion
      
END MODULE version_m   