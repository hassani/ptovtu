!---------------------------------------------------------------------------------------------
! ptovtu, version 2022.7.0
!---------------------------------------------------------------------------------------------
!
! Module name: 
!       utilptovtu_m
!
! Description: 
!       
! 
! Notes: 
!       
!
! Author(s): 
!        R. Hassani, Universite Côte d'Azur
!
! Start date: 
!        06/22
!
! Changes:
!        07/22
!        08/22
!
!---------------------------------------------------------------------------------------------

#include "error.fpp"

MODULE utilptovtu_m

   use globalParameters_m
  
   implicit none
   
CONTAINS
   
!=============================================================================================   
   SUBROUTINE utilptovtu_writeVtu
!============================================================================================= 

!---------------------------------------------------------------------------------------------
!  Writes the f.e. solution to a vtu file. 
!  It uses the excellent VtkFortran library of Stefano Zaghi
!---------------------------------------------------------------------------------------------

!- local variables: --------------------------------------------------------------------------   
   character(len=*), parameter :: HERE = 'utilptovtu_writeVtu'
   logical         , parameter :: YES = .true., NO = .false.   
   integer                     :: e
!---------------------------------------------------------------------------------------------
!print*,'in '//HERE

   if ( currentStep /= nextStep ) then
      write(*,'(a)')'<= step NOT writen' 
      return
   end if
!
!- Open the vtu file:
!
   fileout = nameVtuDir // filein //'.'//util_intToChar(currentStep)//'.vtu'
   write(*,'(a,a)')'<= Writing to file ',fileout

   e = fvtu%initialize ( format=vtuFileFmt, filename=fileout, & 
                           mesh_topology='UnstructuredGrid')
                           
   if ( e > 0 ) then
      stat = err_t ( stat = UERROR, where = HERE, &
                      msg = 'vtkFortran error (action: "initialize")' )
      return
   end if

   e = fvtu%xml_writer%write_piece ( np = int(mesh%npoin,i32), &
                                     nc = int(mesh%ncell,i32)  )            
   if ( e > 0 ) then
      stat = err_t ( stat = UERROR, where = HERE, &
                      msg = 'vtkFortran error (action: "write_piece" (start))' )
      return
   end if
!
!- mesh connectivity and nodal coordinates
!
   call mesh%writeVtu ( fvtu, stat )
   error_TraceNreturn(stat>IZERO, HERE, stat) 
!
!- nodal values:
!
   if ( vals(NODV) % is_active ) then
      call vals(NODV) % writeVtu ( loc='node', open=YES, close=YES, fvtu=fvtu, stat=stat )
      error_TraceNreturn(stat>IZERO, HERE, stat)
   end if
!
!- element values:
!
   if ( vals(ELMV) % is_active  ) then
      if ( vals(ELMI) % is_active  ) then
         call vals(ELMV) % writeVtu (loc='cell', open=YES , close=NO, fvtu=fvtu, stat=stat) 
         error_TraceNreturn(stat>IZERO, HERE, stat)      
         call vals(ELMI) % writeVtu (loc='cell', open=NO , close=YES, fvtu=fvtu, stat=stat) 
         error_TraceNreturn(stat>IZERO, HERE, stat) 
      else
         call vals(ELMV) % writeVtu (loc='cell', open=YES , close=YES, fvtu=fvtu, stat=stat) 
         error_TraceNreturn(stat>IZERO, HERE, stat)   
      end if
   end if
!
!- End writing:
!   
   e = fvtu%xml_writer%write_piece ()  
    
   if ( e > 0 ) then
      stat = err_t ( stat = UERROR, where = HERE, &
                      msg = 'vtkFortran error (action: "write_piece" (end))' )
      return
   end if
!
!- Close the vtu file:
! 
   e = fvtu%finalize ()   ! I don't know why e > 0 when raw format are requested,
                          ! however the generated vtu files look and work fine 
                          ! --> Ask S. Zaghi on occasion
!    if ( e > 0 ) then
!       stat = err_t ( stat = UERROR, where = HERE, &
!             msg = 'vtkFortran error (action: "finalize"). Code error: '//util_intToChar(e) )
!       return
!    end if

!
!- The step # for the next write:
!
   nextStep = nextStep + incremStep 
   if ( nextStep > finalStep ) then
      stat = err_t ( stat = EOF, where = HERE, msg = '(final requested step reached)' )
      return
   end if

!print*,'end '//HERE           
   END SUBROUTINE utilptovtu_writeVtu


!=============================================================================================   
   SUBROUTINE utilptovtu_getCommandArg
!=============================================================================================   

!---------------------------------------------------------------------------------------------
!  Gets the command line arguments (retrieve user-entered options)
!---------------------------------------------------------------------------------- RH 06/22 -
   
!- local variables: --------------------------------------------------------------------------   
   character(len=*), parameter   :: HERE = 'utilptovtu_getCommandArg', MARK = char(192)
   integer                       :: i, pos, narg, pval
   character(len=:), allocatable :: opt, rec, lhs
   character(LGSTR)              :: msg
   logical                       :: exists, remove = .false.
   integer                       :: lfile
!---------------------------------------------------------------------------------------------
   
   vtuFileFmt = 'raw' ! default output vtu format
   
   filein = ''
   nameVtuDir = ''

   currentStep = IZERO      
   firstStep   = IONE
   incremStep  = IONE
   finalStep   = huge(finalStep)
   nextStep    = firstStep
!
!- See if a user default file exists on the current directory or on the home directory (the
!  name of this file is filedef, initialized to FDEF in the version subroutine)
!
   inquire (file = filedef, exist = exists)   

   if ( .not. exists ) then   
      call get_environment_variable (name = 'HOME', length = lfile)
      allocate(character(len=lfile) :: opt)
      call get_environment_variable (name = 'HOME', value = opt)
      filedef = trim(adjustl(opt))//'/'//filedef
      inquire (file = filedef, exist = exists)
      ! if not, create one on the home directory:
      if ( .not. exists ) call utilptovtu_createUserDefaultFile ( filedef )
   end if
!
!- get the command line arguments:
!      
   narg = command_argument_count()
   
   if ( narg == 0 ) then
      ! just print the help and terminate:
      call utilptovtu_PrintHelp ()
      stop
   endif
 
   do i = 1, narg   
      call get_command_argument( i, msg )
      opt = trim(adjustl(msg))
         
      if ( index(opt,'-') == 0 ) then
         if ( len(filein) == 0 ) then 
            filein = opt
         else
            call utilptovtu_error ( err = UERROR, where = HERE,              &   
                                    msg = 'Unknown "'//trim(opt)//'" option' )
            return
         end if
         
      else if ( opt == '-no_conv' ) then
         convert = .false.
      
      else if ( index(opt,'-fmt') == 1 ) then
         pval = len('-fmt=') + 1
         if ( index(opt,'-fmt=') == 1 .and. len(opt) >= pval ) then
            vtuFileFmt = opt(pval:)
         else
            call utilptovtu_error ( err=UERROR, where= HERE, msg='Bad use of "-fmt=" option' )
            return
         end if
         
      else if ( opt == '-h' ) then
         call utilptovtu_printHelp ()
         if ( narg == 1 ) stop
         
      else if ( opt == '-v' ) then
         call utilptovtu_printVersion ()
         if ( narg == 1 ) stop
      
      else if ( opt == '-rm' ) then
         remove = .true.
         
      else if ( index(opt,'-dir') == IONE ) then
         pval = len('-dir=') + 1
         if ( index(opt,'-dir=') == 1 .and. len(opt) >= pval ) then
            nameVtuDir = opt(pval:)
         else if ( opt == '-dir' ) then
            nameVtuDir = MARK
         else
            call utilptovtu_error ( err=UERROR, where= HERE, msg='Bad use of "-dir=" option' )
            return
         end if
            
      else if ( index(opt,'-range') == 1 ) then
         pval = len('-range=') + 1
         if ( index(opt,'-range=') == 1 .and. len(opt) >= pval ) then
            rec = trim(adjustl(opt(pval:)))
            pos = index(rec,':')
            if ( pos == 0 ) then
               incremStep = IONE
               read(rec,*,iostat=iostat) firstStep               
               if (iostat == IZERO) finalStep = firstStep
            else
               lhs = trim(adjustl(rec(:pos-1)))
               read(lhs,*,iostat=iostat) firstStep
               if ( iostat == IZERO ) then      
                  rec = trim(adjustl(rec(pos+1:)))
                  pos = index(rec,':')
                  if ( pos == 0 ) then
                     incremStep = IONE
                     read(rec,*,iostat=iostat) finalStep
                  else
                     lhs = trim(adjustl(rec(:pos-1)))
                     rec = trim(adjustl(rec(pos+1:)))
                     read(lhs,*,iostat=iostat) incremStep
                     if ( iostat == IZERO ) read(rec,*,iostat=iostat) finalStep
                  endif
               end if   
            end if      
            if ( iostat /= IZERO ) then
               call utilptovtu_error ( err = UERROR, where = HERE,                  &   
                       msg = "The values of the '-range' argument must be integers" )
               return
            end if
         else
            call utilptovtu_error ( err = UERROR, where = HERE,          &
                                    msg = 'Bad use of "-range=" option ' )
            return
         end if

         if ( firstStep > IZERO ) then
            nextStep = firstStep
         else
            nextStep = incremStep
         end if
               
      else
         call utilptovtu_error ( err = UERROR, where = HERE,              &   
                                 msg = 'Unknown "'//trim(opt)//'" option' )
         return
      end if
      
   end do       

   if ( remove ) then
      if ( len(filein) == 0 ) then
         call utilptovtu_error ( err = UERROR, where = HERE,  &   
                                 msg = '-rm needs a file name')
         return
      end if
      if ( len(nameVtuDir) > 0 ) then
         if ( nameVtuDir == MARK ) nameVtuDir = 'vtu_' // filein
         filein = nameVtuDir // '/' // trim(adjustl(filein)) // '*.vtu'
      else
         filein = trim(adjustl(filein)) // '.*.vtu'
      endif
      
      call utilptovtu_executeCommandLine ( '/bin/rm -v -f '//filein, stat, &
                                           msg = 'removing the file(s):')!//filein//':'  )
      error_TraceNreturn(stat>IZERO, HERE, stat)
      if ( len(nameVtuDir) > IZERO ) then 
         call utilptovtu_executeCommandLine ( '/bin/rmdir '//nameVtuDir, stat, &
                                         'removing the directory: '//nameVtuDir)
      end if                                  
      stop
   end if
      
   if ( len(filein) == 0 ) stop ! no input file given: terminate
   
   if ( vtuFileFmt /= 'bin' .and. vtuFileFmt /= 'ascii' .and. vtuFileFmt /= 'raw' ) then
      call utilptovtu_error ( err = UERROR, where = HERE,                   &   
                          msg = 'Invalid output format "' // vtuFileFmt //  &                
                                '". Choose from: bin, ascii, raw'           )
      return
   else if ( vtuFileFmt == 'bin' ) then
      vtuFileFmt = 'binary'
   end if
   
   if ( len(nameVtuDir) > 0 ) then
      ! create the directory where the vtu file will be placed (if it does not already exist) 
      if ( nameVtuDir == MARK ) nameVtuDir = 'vtu_' // filein
      call utilptovtu_executeCommandLine ( 'mkdir -p '//nameVtuDir, stat )
      error_TraceNreturn(stat>IZERO, HERE, stat)
      nameVtuDir = nameVtuDir // '/'
   end if
      
   END SUBROUTINE utilptovtu_getCommandArg


!=============================================================================================   
   SUBROUTINE utilptovtu_userDefault
!=============================================================================================   
   use calmat_m, only: calmat, G_FREE, G_objs, G_vars, G_nobj, G_nvar

!---------------------------------------------------------------------------------------------
!  Reads and parses the user's default file. Initializes the catalog of physical quantities,
!  the lists of the requested invariants and eigen-elements.
!-------------------------------------------------------------------------- RH 06/22 - 07/22 -
   
!- local variables: --------------------------------------------------------------------------   
   character(len=*), parameter   :: HERE = 'utilptovtu_userDefault'
   integer  (Ikind)              :: i, j, k, n, m, var
   character(len=:), allocatable :: str
   type     (str_t), allocatable :: vecS1(:), vecS2(:)
   logical                       :: units, table, operator, choosen   
!---------------------------------------------------------------------------------------------
!print*,'in '//HERE

   if ( stat > IZERO ) return
!
!- Use calmat to read and parse the default file:
! 
   call calmat ( fileIn = filedef, warning = .true.,  &
                 welcome = .false., dispRes = .false., stat = stat )  
   error_TraceNreturn(stat>IZERO, HERE, stat)   

   call SignalHandler_SignalCatch (unit = STDOUT, title = '--> ptovtu Info:')   
!
!- Get from the calmat objects the definition of the physical quantities and their units:
!
   n = IZERO
   do i = 1, G_nObj
      if ( G_objs(i)%getStatus() == G_FREE ) cycle
      n = n + IONE
   end do
   allocate(physQuantCatalog(n))

   n = IZERO
   do i = 1, G_nobj
      if ( G_objs(i)%getStatus() == G_FREE ) cycle
      n = n + IONE
      
      units = .false. ; table = .false. ; operator = .false. ; choosen = .false.
      do j = 1, G_objs(i)%getCmpNum()
         var = G_objs(i)%getVarId(j)
         str = G_vars(var)%getName()
         k = index(str,'.')
         str = trim(adjustl(str(k+1:)))
         select case ( str )
            case ( 'units' )
               call G_vars(var)%GetMatPacked ( physQuantCatalog(n)%unitNames )
               units = .true.
            case ( 'conversionTable' )
               call G_vars(var)%GetMatPacked ( physQuantCatalog(n)%conversionTable )
               table = .true.
            case ( 'conversionOperator' )
               call G_vars(var)%GetMatPacked ( vecS1 )
               physQuantCatalog(n)%conversionOperator = vecS1(1)%str
               operator = .true.
            case ( 'choosenUnit' ) 
               call G_vars(var)%GetMatPacked ( vecS2 )
               choosen = .true.
         end select
      end do

      if ( units .and. table .and. operator .and. choosen ) then
         physQuantCatalog(n)%is_init = .true.
         physQuantCatalog(n)%quantityName = G_objs(i)%getName()    
      else
         cycle  
      end if
      
      m = size(physQuantCatalog(n)%unitNames)
      if ( m /= size(physQuantCatalog(n)%conversionTable) ) then
         stat = err_t ( msg = 'Arrays "'//physQuantCatalog(n)%quantityName//'.units" ' // &       
                       'and "'//physQuantCatalog(n)%quantityName//'.conversionTable" ' // &
                       'are of different size',stat = UERROR, where = HERE ) 
         return   
      end if
   
      do j = 1, m
         if ( physQuantCatalog(n)%unitNames(j) == vecS2(1) ) then
            physQuantCatalog(n)%choosenUnit = j
            exit
         end if
      end do
   
      if ( physQuantCatalog(n)%choosenUnit == IZERO ) then
         stat = err_t ( msg = 'Unexpected choosen unit "'// vecS2(1)%str // '" for ' // &
                       'physical quantity "'// physQuantCatalog(n)%quantityName // '"', &
                        stat = UERROR, where = HERE )    
         return   
      end if
   
   end do

   call units_printCatalog ( physQuantCatalog, title = &
                            'Catalog of physical quantities and their choosen units'//NL// &
                            '(see your "'//filedef//'" file):' )   
!
!- Get from the calmat variables the list of invariants and eigenvalues requested by the user:
!   
   nUserInv = IZERO ; nUserEig = IZERO
   k = IZERO
   do i = 1, G_nvar
      if ( G_vars(i)%getName() == 'invariants' ) then
         call G_vars(i)%GetMat ( userListInv )
         k = k + IONE
      end if
      if ( G_vars(i)%getName() == 'eigenvalues' ) then
         call G_vars(i)%GetMat ( userListEig )
         k = k + IONE
      end if
      if ( k == 2 ) exit
   end do
   
   if ( allocated(userListInv) ) then
      nUserInv = size(userListInv,1)
      if ( size(userListInv,2) /= 3 ) then
         stat = err_t ( msg = 'Wrong dimensions of the array "invariants, ' // &       
                       'it must have 3 columns',stat = UERROR, where = HERE  ) 
         return
      end if
      do j = 1, 3
         do i = 1, nUserInv
            userListInv(i,j)%str = trim(adjustl(userListInv(i,j)%str))
         end do
      end do
   end if
   
   if ( allocated(userListEig) ) then
      nUserEig = size(userListEig,1)
      if ( size(userListEig,2) /= 3 ) then
         stat = err_t ( msg = 'Wrong dimensions of the array "eigenvalues, ' // &       
                       'it must have 3 columns',stat = UERROR, where = HERE   ) 
         return
      end if
      do j = 1, 3
         do i = 1, nUserEig
            userListEig(i,j)%str = trim(adjustl(userListEig(i,j)%str))
         end do
      end do
   end if
   
   do i = 1, G_nvar
      if ( G_vars(i)%getName() == 'wildcard' ) then
         call G_vars(i)%GetMatPacked ( vecS1 )
         wildcard = vecS1(1)%str
         exit
      end if
   end do
   
   END SUBROUTINE utilptovtu_userDefault
   
   
!=============================================================================================   
   SUBROUTINE utilptovtu_InvariantsNEigenvalues ( )
!=============================================================================================  
!---------------------------------------------------------------------------------------------
!  Computes invariants or/and eigenvalues and eigenvectors of cell centered tensors
!
!  The list of requested invariants (userListInv) and the requested eigenvalues (userListEig)
!  were extracted from the user default file in the subroutine ptovtu_userDefault
!---------------------------------------------------------------------------------------------

!- local variables --------------------------------------------------------------------------- 
   character(len=*),       parameter   :: HERE = 'utilptovtu_InvariantsNEigenvalues'
   real     (Rkind),       parameter   :: TWO = 2.0_Rkind, THIRD = 1.0_Rkind/3.0_Rkind, &
                                          THREEHALF = 1.50_Rkind, TWOTHIRD = two*third
   logical         ,       parameter   :: NO = .false., YES = .true.
   logical                             :: is_available
   integer  (Ikind)                    :: i, j, k, n, ivar, inlist, nvar, ncell
   character(len=:),       allocatable :: msg
!- reused ------------------------------------------------------------------------------------
   integer  (Ikind), save              :: ninv, neig
   integer  (Ikind), save, allocatable :: occursInv(:), numListInv(:,:), numInv(:,:), &
                                          occursEig(:), numListEig(:,:), numEig(:,:)
   real     (Rkind), save, allocatable :: sig1(:), sig2(:), sig3(:), tens(:), invar(:), &
                                          dir1(:), dir2(:), dir3(:)  
   type     (str_t), save, allocatable :: availableInvar(:)                                        
!---------------------------------------------------------------------------------------------

   if ( nUserInv == IZERO .and. nUserEig == IZERO ) return   
   
   if ( .not. allocated(availableInvar) ) call utilptovtu_invar ( listvar = availableInvar )
   
   if ( vals(ELMV)%is_new ) then
!
!-    See if the requested invariants involve variables that are present in the list of
!     element values vals(ELMV) and that are symmetric tensors (6 comp.). 
!     Otherwise ignore them.
!     Do the same thing for eigenvalues.

      nvar = vals(ELMV)%nvar
      
      if ( allocated(occursInv) ) then
         if ( size(occursInv) < nvar ) &
            deallocate( occursInv, numListInv, numInv, occursEig, numListEig, numEig )
      end if
      if ( .not. allocated(occursInv) ) &
         allocate( occursInv(nvar), numListInv(nUserInv,nvar), numInv(nUserInv,nvar), &
                   occursEig(nvar), numListEig(nUserEig,nvar), numEig(nUserEig,nvar), &
                   source = 0_Ikind )
      
      ninv = IZERO ; neig = IZERO
      
      do ivar = 1, nvar
         
         if ( vals(ELMV)%varNcomp(ivar) /= 6 ) cycle ! it is not a sym. tensor
         
         do inList = 1, nUserInv
            if ( vals(ELMV)%varNames(ivar) /= userListInv(inList,2)%str ) cycle
            
            is_available = .false.
            do i = 1, size(availableInvar)
               if ( userListInv(inList,1)%str == availableInvar(i)%str ) then
                  is_available = .true.
                  exit
               end if
            end  do
            if ( .not. is_available ) then
               msg = availableInvar(1)%str
               do i = 2, size(availableInvar)
                  msg = msg // ', '//availableInvar(i)%str
               end do
               call utilptovtu_error ( err = WARNING, where = HERE,                 &
                    msg = 'Unknown invariant "'//userListInv(inList,1)%str //'" '// &
                          'found in ' // filedef // NLT //                          &
                          'Available invariants are: '// NLT // msg                 )
               cycle
            end if
            
            ninv = ninv + IONE
            occursInv(ivar) = occursInv(ivar) + IONE ! number of times the variable is involved
            numListInv(occursInv(ivar),ivar) = inList
            numInv    (occursInv(ivar),ivar) = ninv   
         end do
         
         do inList = 1, nUserEig
            if ( vals(ELMV)%varNames(ivar) /= userListEig(inList,2)%str ) cycle
            neig = neig + IONE
            occursEig(ivar) = occursEig(ivar) + IONE ! number of times the variable is involved
            numListEig(occursEig(ivar),ivar) = inList
            numEig    (occursEig(ivar),ivar) = neig  
         end do         
      end do

      if ( ninv == IZERO .and. neig == IZERO ) then
         return
      else 
         ncell = vals(ELMV)%nent
         
         if ( ninv /= IZERO ) then
            if ( allocated(invar) ) then
               if ( size(invar) < ncell ) deallocate( invar )
            end if
            if ( .not. allocated(invar) ) allocate( invar(ncell) )
         end if
         
         if ( neig /= IZERO ) then
            if ( allocated(sig1) ) then
               if ( size(sig1) < ncell ) deallocate( sig1, dir1, sig2, dir2, sig3, dir3 )
            end if
            if ( .not. allocated(sig1) ) allocate( sig1(ncell), dir1(3*ncell), &
                                                   sig2(ncell), dir2(3*ncell), &
                                                   sig3(ncell), dir3(3*ncell)  )
         end if
         if ( allocated(tens) ) then
            if ( size(tens) < 6*ncell ) deallocate( tens )
         end if
         if ( .not. allocated(tens) ) allocate( tens(ncell) )
      end if
          
      ! total of variables is: ninv invariants + 3*neig eigenvalues + 3*neig eigenvectors 
      
      call vals(ELMI)%alloc ( nvar = ninv+6*neig , nent = ncell ) 
   
      do ivar = 1, nvar
         do i = 1, occursInv(ivar)
            k = numInv(i,ivar)
            inList = numListInv (i,ivar)
            vals(ELMI)%physNames (k) = vals(ELMV)%physNames (ivar)
            vals(ELMI)%physUnits (k) = vals(ELMV)%physUnits (ivar)
            vals(ELMI)%physUnits0(k) = vals(ELMV)%physUnits0(ivar)
            
            vals(ELMI)%varNames  (k) = userListInv(inList,3)%str
            vals(ELMI)%varNcomp  (k) = IONE
         
            vals(ELMI)%convUnit  (k) = vals(ELMV)%convUnit(ivar)
            vals(ELMI)%convOper  (k) = vals(ELMV)%convOper(ivar)
            vals(ELMI)%please_convertMe(k) = vals(ELMV)%please_convertMe(ivar)
            
         end do
         
         do i = 1, occursEig(ivar)
            k = (numEig(i,ivar)-1)*6
            inList = numListEig (i,ivar)
            do j = 1, 3
               k = k + IONE
               vals(ELMI)%physNames (k+ninv) = vals(ELMV)%physNames (ivar)
               vals(ELMI)%physUnits (k+ninv) = vals(ELMV)%physUnits (ivar)
               vals(ELMI)%physUnits0(k+ninv) = vals(ELMV)%physUnits0(ivar)
            
               vals(ELMI)%varNames  (k+ninv) = userListEig(inList,3)%str // &
                                               util_intTochar(j)
               vals(ELMI)%varNcomp  (k+ninv) = IONE

               vals(ELMI)%convUnit(k+ninv) = vals(ELMV)%convUnit(ivar)
               vals(ELMI)%convOper(k+ninv) = vals(ELMV)%convOper(ivar)
               vals(ELMI)%please_convertMe(k+ninv) = vals(ELMV)%please_convertMe(ivar)
         
            end do
            k = (numEig(i,ivar)-1)*6 + 3
            do j = 1, 3
               k = k + IONE
               vals(ELMI)%physNames (k+ninv) = '-'
               vals(ELMI)%physUnits (k+ninv) = '-'
               vals(ELMI)%physUnits0(k+ninv) = '-'
            
               vals(ELMI)%varNames  (k+ninv) = 'dir_'// userListEig(inList,3)%str // &
                                                util_intTochar(j)
               vals(ELMI)%varNcomp  (k+ninv) = 3
         
               vals(ELMI)%please_convertMe(k+ninv) = .false.
            end do            
         end do         
      end do
               
      call vals(ELMI)%alloc ( allocv = .true. )
             
      vals(ELMI)%geomEntities = 'cells'

      vals(ELMI)%is_new =.true.
   else
      vals(ELMI)%is_new = .false.
   end if
   
   if ( ninv == IZERO .and. neig == IZERO ) return
   
   vals(ELMI)%is_active =.true.

   ncell = vals(ELMV)%nent

   do ivar = 1, vals(ELMV)%nvar
      if ( occursInv(ivar) == IZERO .and. occursEig(ivar) == IZERO ) cycle
      
      call vals(ELMV) % get ( varId = ivar, values = tens, stat = stat )
      error_TraceNreturn(stat>IZERO, HERE, stat) 
            
      if ( ninv /= IZERO ) then
         do i = 1, occursInv(ivar)
            n = numInv(i,ivar)
            inList = numListInv (i,ivar)

            call utilptovtu_invar ( tens(1:6*ncell), userListInv(inList,1)%str, invar )

            call vals(ELMI) % set ( varId = n, values = invar(1:ncell), stat = stat ) 
            error_TraceNreturn(stat>IZERO, HERE, stat) 
         end do
      end if
      
      if ( neig /= IZERO ) then
         do i = 1, occursEig(ivar)
            n = 6*(numEig(i,ivar)-1)
            inList = numListEig (i,ivar)

            call utilptovtu_eigen ( tens(1:6*ncell), userListEig(inList,1)%str, &
                                    sig1(1:ncell)  , dir1(1:3*ncell),           &
                                    sig2(1:ncell)  , dir2(1:3*ncell),           &
                                    sig3(1:ncell)  , dir3(1:3*ncell)            )                       

            call vals(ELMI) % set ( varId = ninv+n+1, values = sig1(1:ncell)  , stat = stat )            
            call vals(ELMI) % set ( varId = ninv+n+2, values = sig2(1:ncell)  , stat = stat )            
            call vals(ELMI) % set ( varId = ninv+n+3, values = sig3(1:ncell)  , stat = stat )  
            call vals(ELMI) % set ( varId = ninv+n+4, values = dir1(1:3*ncell), stat = stat )            
            call vals(ELMI) % set ( varId = ninv+n+5, values = dir2(1:3*ncell), stat = stat )            
            call vals(ELMI) % set ( varId = ninv+n+6, values = dir3(1:3*ncell), stat = stat ) 
            error_TraceNreturn(stat>IZERO, HERE, stat) 
         end do    
      end if  
   end do
   
   END SUBROUTINE utilptovtu_InvariantsNEigenvalues


!=============================================================================================   
   SUBROUTINE utilptovtu_invar ( tens, name, invar, listvar )
!=============================================================================================   
   real     (Rkind),              optional, intent(in    ) :: tens(:)
   character(len=*),              optional, intent(in    ) :: name
   real     (Rkind),              optional, intent(in out) :: invar(:)
   type     (str_t), allocatable, optional, intent(   out) :: listvar(:)
!---------------------------------------------------------------------------------------------

!- local variables --------------------------------------------------------------------------- 
   real   (Rkind), parameter :: THIRD = 1.0_Rkind/3.0_Rkind, TWO = 2.0_Rkind,   &
                                HALF = 0.5_Rkind, THREEHALF = 1.50_Rkind, TWOTHIRD = TWO*THIRD
   real   (Rkind)            :: p, norm2
   integer(Ikind)            :: e, k, ncell
!---------------------------------------------------------------------------------------------

   if ( present(listvar) ) then
      allocate(listvar(6))
      listvar(1) = 'volPart'
      listvar(2) = 'isoPart'
      listvar(3) = 'sqrtI2'
      listvar(4) = 'sqrtJ2'
      listvar(5) = 'equivStress'
      listvar(6) = 'equivStrain'
      return
   end if
   
   if ( .not. present(tens) .or. .not. present(name) .or. .not. present(invar) ) return
   
   ncell = size(tens) / 6
   
   select case ( name )   
      case ( 'volPart' )
         do e = 1, ncell
            k = (e-1)*6
            invar(e) = tens(k+1) + tens(k+2) + tens(k+3)
         end do
      case ( 'isoPart' )
         do e = 1, ncell
            k = (e-1)*6
            invar(e) = ( tens(k+1) + tens(k+2) + tens(k+3) ) * THIRD
         end do     
      case ( 'sqrtI2' )
         do e = 1, ncell
            k = (e-1)*6
            norm2 = tens(k+1)**2 + tens(k+2)**2 + tens(k+3)**2 + &
                    TWO * (tens(k+4)**2 + tens(k+5)**2 + tens(k+6)**2)
            invar(e) = sqrt( HALF * norm2 ) 
         end do        
      case ( 'sqrtJ2' )
         do e = 1, ncell
            k = (e-1)*6
            p = ( tens(k+1) + tens(k+2) + tens(k+3) ) * THIRD
            norm2 = (tens(k+1)-p)**2 + (tens(k+2)-p)**2 + (tens(k+3)-p)**2 + &
                     TWO * (tens(k+4)**2 + tens(k+5)**2 + tens(k+6)**2)
            invar(e) = sqrt( HALF * norm2 )  
         end do                   
      case ( 'equivStress' )
         do e = 1, ncell
            k = (e-1)*6
            p = ( tens(k+1) + tens(k+2) + tens(k+3) ) * THIRD
            norm2 = (tens(k+1)-p)**2 + (tens(k+2)-p)**2 + (tens(k+3)-p)**2 + &
                     TWO * (tens(k+4)**2 + tens(k+5)**2 + tens(k+6)**2)
            invar(e) = sqrt( THREEHALF * norm2 )
         end do                        
      case ( 'equivStrain' )
         do e = 1, ncell
            k = (e-1)*6
            p = ( tens(k+1) + tens(k+2) + tens(k+3) ) * THIRD
            norm2 = (tens(k+1)-p)**2 + (tens(k+2)-p)**2 + (tens(k+3)-p)**2 + &
                     TWO * (tens(k+4)**2 + tens(k+5)**2 + tens(k+6)**2)
            invar(e) = sqrt( TWOTHIRD * norm2 )
         end do                                    
   end select
   
   END SUBROUTINE utilptovtu_invar


!=============================================================================================
   SUBROUTINE utilptovtu_eigen ( tens, name, sig1, dir1, sig2, dir2, sig3, dir3 )
!=============================================================================================
   real     (Rkind), intent(in    ) :: tens(:)
   character(len=*), intent(in    ) :: name
   real     (Rkind), intent(   out) :: sig1(:), dir1(:), sig2(:), dir2(:), sig3(:), dir3(:)
!---------------------------------------------------------------------------------------------  
!  Computes eigenvalues and eigenvectors of a symmetric 3x3 tensor field or of its deviator.
!
!  Inputs:
!
!  . tens: real array of size 6 * ncell containing the 6 independent components of the tensor
!          for each cell. These components are stored in tens in the order
!                xx, yy, zz, xy, yz, zx | xx, yy, zz, xy, yz, zx |  ...
!                     (first cell)           (second cell)         (...)
!
!  . name: may be 'whole' or 'deviator'
!
!  Outputs:
!
!  . sig1, sig2, sig3: the eigenvalues sorted in increasing order (sig1 <= sig2 <= sig3)
!  . dir1, dir2, dir3: the corresponding eigenvectors
!  
!  Called routine: 
!
!  . utilptovtu_eigvec3x3 (alias diag3x3) by Sebastian Ehlert
!  . alternatively utilptovtu_eigenMatSym3 which uses dsyevj3 by Joachim Kopp
!---------------------------------------------------------------------------------------------  

!- local variables: --------------------------------------------------------------------------
   real   (Rkind), parameter :: THIRD = 1.0_Rkind/3.0_Rkind  
   real   (Rkind)            :: p, mat(3,3), valp(3), vecp(3,3)
   integer(Ikind)            :: e, k, j, ncell
!---------------------------------------------------------------------------------------------    

   ncell = size(tens) / 6
   
   select case ( name )   
      case ( 'whole' )
         do e = 1, ncell
            k = (e-1)*6 ; j = (e-1)*3
            mat(:,1) = [tens(k+1)  , tens(k+4)  , tens(k+6)  ]
            mat(:,2) = [tens(k+4)  , tens(k+2)  , tens(k+5)  ]
            mat(:,3) = [tens(k+6)  , tens(k+5)  , tens(k+3)  ]  
            
            call utilptovtu_eigvec3x3 ( mat, valp, vecp )
            
            sig1(e) = valp(1) ; dir1(j+1:j+3) = vecp(:,1)
            sig2(e) = valp(2) ; dir2(j+1:j+3) = vecp(:,2)
            sig3(e) = valp(3) ; dir3(j+1:j+3) = vecp(:,3) 
!            call utilptovtu_eigenMatSym3 ( mat, sig1(e), sig2(e), sig3(e),             &
!                                           dir1(j+1:j+3), dir2(j+1:j+3), dir3(j+1:j+3) )  
         end do                        
      case ( 'deviator' )
         do e = 1, ncell
            k = (e-1)*6 ; j = (e-1)*3
            p = ( tens(k+1) + tens(k+2) + tens(k+3) ) *  THIRD
            mat(:,1) = [tens(k+1)-p, tens(k+4)  , tens(k+6)  ]
            mat(:,2) = [tens(k+4)  , tens(k+2)-p, tens(k+5)  ]
            mat(:,3) = [tens(k+6)  , tens(k+5)  , tens(k+3)-p]
             
            call utilptovtu_eigvec3x3 ( mat, valp, vecp )
            
            sig1(e) = valp(1) ; dir1(j+1:j+3) = vecp(:,1)
            sig2(e) = valp(2) ; dir2(j+1:j+3) = vecp(:,2)
            sig3(e) = valp(3) ; dir3(j+1:j+3) = vecp(:,3)                        
!            call utilptovtu_eigenMatSym3 ( mat, sig1(e), sig2(e), sig3(e),             &
!                                           dir1(j+1:j+3), dir2(j+1:j+3), dir3(j+1:j+3) )                 
         end do                                    
   end select

   END SUBROUTINE utilptovtu_eigen


!=============================================================================================   
   SUBROUTINE utilptovtu_executeCommandLine ( expr, stat, msg )
!=============================================================================================   
   character(len=*),           intent(in    ) :: expr
   type     (err_t),           intent(in out) :: stat
   character(len=*), optional, intent(in    ) :: msg
!---------------------------------------------------------------------------------------------    
!  Calls execute_command_line
!---------------------------------------------------------------------------------------------

!- local variables --------------------------------------------------------------------------- 
   character(len=*    ), parameter :: HERE = 'utilptovtu_executeCommandLine'  
   character(len=LGSTR)            :: cmdmsg
   integer                         :: exitstat, cmdstat   
!---------------------------------------------------------------------------------------------

   if ( present(msg) ) write(*,'(a)') trim(msg)
   
   cmdmsg = ''  ; exitstat = 0    
   call execute_command_line ( expr, exitstat=exitstat, cmdstat=cmdstat, cmdmsg=cmdmsg )
   
   if ( cmdstat > 0 ) then
      if ( len_trim(cmdmsg ) == 0) then
         cmdmsg = 'Command execution failed '
      else
         cmdmsg = 'Command execution failed with the error "'//trim(cmdmsg)//'"'
      end if
      stat = err_t ( stat=UERROR, where=HERE, msg=cmdmsg )
      
   else if ( cmdstat < 0 ) then
      stat = err_t ( stat=UERROR, where=HERE, msg='Command line execution not supported' )
      
   else if (exitstat /= 0) then
      if ( len_trim(cmdmsg) == 0 ) then
         cmdmsg = 'Command execution failed '
      else
         cmdmsg = 'Command execution failed with the error "'//trim(msg)//'"'
      end if
      stat = err_t ( stat=UERROR, where=HERE, msg=cmdmsg )
      
   end if

   END SUBROUTINE utilptovtu_executeCommandLine
   

!============================================================================================= 
   SUBROUTINE utilptovtu_printVersion  
!=============================================================================================   
   use, intrinsic :: iso_fortran_env, only : compiler_version, compiler_options
!---------------------------------------------------------------------------------------------

!- local variables: --------------------------------------------------------------------------   
!---------------------------------------------------------------------------------------------

   write(*,'(/,a)')  "                    ptovtu program -- Version Information"
   write(*,'(  a)')  "                    ====================================="

   write(*,'(a)')    "    . Version number       "//numVersion
   write(*,'(a)')    "    . Compiler name        "//trim(adjustl(compiler_version()))
   if ( len_trim(compilOpts) /= 0 ) then
      write(*,'(a)') "    . Compilation options  "//trim(adjustl(compilOpts))
   else
      write(*,'(a)') "    . Compilation options  "//trim(adjustl(compiler_options()))
   end if
   write(*,'(a)')    "    . Compilation date     "//trim(adjustl(compilDate))
   write(*,'(a)')    "    . Default integers     "//util_intToChar(Ikind*8)//'-bits'
   write(*,'(a)')    "    . Default reals        "//util_intToChar(Rkind*8)//'-bits'
   write(*,'(a,/)')  "    . Libraries            pk2, calmat, VtkFortran"
   
   END SUBROUTINE utilptovtu_printVersion


!============================================================================================= 
   SUBROUTINE utilptovtu_printHelp  
!=============================================================================================   
!---------------------------------------------------------------------------------------------

!- local variables: --------------------------------------------------------------------------   
!---------------------------------------------------------------------------------------------

   write(*,'(/,a)') "                             ptovtu program -- Help"
   write(*,'(  a)') "                             ======================"
   
   write(*,'(a)') "Usage:"


   write(*,'(a,/)'  ) &
   "   ptovtu [ name -dir[=] -fmt={bin, raw, ascii} -h -no_conv -range={n, n:m, n:s:m} -rm -v ]"
      
   write(*,'(a)'   ) &
   "   where  name is the input file name.  Options and name can be in any order."

   write(*,'(/,a)' ) "Options:"
   

   write(*,'(/,a)'  ) &
   "   -dir           lets  you  give the name of the directory where the vtu files"
   write(*,'(a)'    ) &
   "                  will be placed"
   write(*,'(a)'    ) &
   "                  o If not used,  the  vtu files will be written to the current"
   write(*,'(a)'    ) &
   "                    directory"   
   write(*,'(a)'    ) &
   "                  o If  -dir  is used with no value the directory will be named"   
   write(*,'(a)'    ) &
   "                    after  the name of the input file preceded by 'vtu_'"
   write(*,'(a)'    ) &
   "                  o Otherwise  -dir=name  specifies the name of the directory"   

      
   write(*,'(/,a)'  ) &
   "   -fmt           lets  you  set  the  output  vtu  format  (bin, raw or ascii)"
   write(*,'(a)'    ) &
   "                  The default is  -fmt=raw"

   write(*,'(/,a)'  ) &
   "   -h             display this help "

   write(*,'(/,a)'  ) &
   "   -no_conv       no unit conversion (keep those used in the p-file). Otherwise"
   write(*,'(a)'    ) &
   "                  conversions  are  done according to those defined in the file"
   write(*,'(a)'    ) &   
   "                  '.ptovtuUserDefaults'"
   
      
   write(*,'(/,a)'  ) &
   "   -range         allows you to select the numbers of the outputs to keep.  The"
   write(*,'(a)'    ) &
   "                  following syntaxes are used:"

   write(*,'(a)'    ) &
   "                  -range=n        only the output #n is selected"

   write(*,'(a)'    ) &
   "                  -range=n:m      all those between #n and #m are selected"

   write(*,'(a)'    ) &
   "                  -range=n:i:m    all those between #n and #m  with increment i"
   write(*,'(a)'    ) &
   "                                  are selected"


   write(*,'(/,a)'  ) &
   "   -rm            remove all the name.*.vtu files "
   write(*,'(a)'    ) &
   "                  o If  -dir is not used, the name.*.vtu files are removed from"
   write(*,'(a)'    ) &
   "                    the current directory"
   write(*,'(a)'    ) &
   "                  o If -dir is used, the name.*.vtu files  are removed from the"
   write(*,'(a)'    ) &
   "                    'vtu_name'  directory  which  is  finally  deleted if empty"
   write(*,'(a)'    ) &
   "                  o If  -dir=dirname is used, these files are removed from  the"
   write(*,'(a)'    ) &
   "                    dirname directory which is finally deleted if empty"
   
   
   write(*,'(/,a)'  ) &
   "   -v             get information about this version "

   write(*,'(/,a)' ) "Examples:"
   write(*,'(/,a)'  ) &
   " ~> ptovtu  pessai"     
   write(*,'(/,a)'  ) &
   " ~> ptovtu  pessai  -fmt=ascii  -range=40"         
   write(*,'(/,a)'  ) &
   " ~> ptovtu  pessai  -fmt=bin  -range=0:2:40  -dir"         
   write(*,'(/,a)'  ) &
   " ~> ptovtu  pessai  -fmt=bin  -range=0:2:40  -dir=mydir"        
   write(*,'(/,a)'  ) &
   " ~> ptovtu  -rm  pessai"        
   write(*,'(/,a)'  ) &
   " ~> ptovtu  pessai -rm -dir"        
   write(*,'(/,a)'  ) &
   " ~> ptovtu  pessai -rm -dir=mydir"        
   write(*,*)
   
   END SUBROUTINE utilptovtu_printHelp
   
   
!=============================================================================================   
   SUBROUTINE utilptovtu_createUserDefaultFile ( fileName )
!============================================================================================= 
   character(len=*), intent(in) :: fileName
!---------------------------------------------------------------------------------------------
!  Writes a user default file containing a catalog of physical quantities with their most 
!  common units.
!  Only the simplest forms of conversion are considered:  
!             X[new unit] = X[old unit] * A    and     Y[new unit] = Y[old unit] + B
!---------------------------------------------------------------------------------------------

!- local variables: -------------------------------------------------------------------------- 
   integer :: ud  
!---------------------------------------------------------------------------------------------
!print*,'in '//HERE

   open(newunit = ud, file = fileName, status = 'replace')

   write(ud,'(a)')'// This file was automatically generated by ptovtu'
   write(ud,'(a)')'// You can modify it to suit your needs'
   write(ud,'(a)')
   write(ud,'(a)')'clear'
   write(ud,'(a)')
   write(ud,'(a)')'///////////////////////////////////////////////////////////////////////////'
   write(ud,'(a)')'//        Catalog of common physical quantities and their units          //'
   write(ud,'(a)')'///////////////////////////////////////////////////////////////////////////'
   write(ud,'(a)')   
   write(ud,'(a)')'// To define a physical quantity, use an object whose name is that of this' 
   write(ud,'(a)')'// quantity and which has the following four members:' 
   write(ud,'(a)')'//  .units: a list of possible units for this quantity' 
   write(ud,'(a)')'//  .conversionTable: conversion factors or terms to convert each referenced '
   write(ud,'(a)')'//                    unit to the 1st (caution: make sure that conversionTable'
   write(ud,'(a)')'//                    is real (use the decimal point if necessary))'
   write(ud,'(a)')'//  .conversionOperator: "*" or "+"'
   write(ud,'(a)')'//  .choosenUnit: the unit to use when writing the vtu files' 
   write(ud,'(a)')      
   write(ud,'(a)')'// Define some variables:'
   write(ud,'(a)')'nano=1e-9; micro=1e-6; milli=1e-3; centi=1e-2; deci=1e-1; unit=1e+0;'
   write(ud,'(a)')'deca=1e+1; hecto=1e+2; kilo =1e+3; myria=1e+4; mega=1e+6; giga=1e+9;'
   write(ud,'(a)')
   write(ud,'(a)')'minut=60; hour=3600; day=24*hour; year=365*day; Myear=mega*year;'
   write(ud,'(a)')
   write(ud,'(a)')' //'
   write(ud,'(a)')' //- For angle:'
   write(ud,'(a)')' //'
   write(ud,'(a)')' angle.units              = ["rad", "deg"  ];'
   write(ud,'(a)')' angle.conversionTable    = [ unit, %pi/180];'
   write(ud,'(a)')' angle.conversionOperator = "*";'
   write(ud,'(a)')
   write(ud,'(a)')' angle.choosenUnit = "deg";'   
   write(ud,'(a)') 

   write(ud,'(a)')'//'
   write(ud,'(a)')'//- For coordinates:'
   write(ud,'(a)')'//'
   write(ud,'(a)')'coordinate.units              = ["m"  , "mm"  , "cm"  , "km" ];'
   write(ud,'(a)')'coordinate.conversionTable    = [ unit,  milli,  centi,  kilo];'
   write(ud,'(a)')'coordinate.conversionOperator = "*";'
   write(ud,'(a)')
   write(ud,'(a)')'coordinate.choosenUnit = "km";'
   write(ud,'(a)')

   write(ud,'(a)')'//'
   write(ud,'(a)')'//- For displacements:'
   write(ud,'(a)')'//'
   write(ud,'(a)')'displacement.units              = ["m"  , "mm"  , "cm"  , "km" ];'
   write(ud,'(a)')'displacement.conversionTable    = [ unit,  milli,  centi,  kilo];'
   write(ud,'(a)')'displacement.conversionOperator = "*";'
   write(ud,'(a)')
   write(ud,'(a)')'displacement.choosenUnit = "cm";'
   write(ud,'(a)')

   write(ud,'(a)')'//'
   write(ud,'(a)')'//- For energy:'
   write(ud,'(a)')'//'
   write(ud,'(a)')'energy.units              = ["J"  , "kJ" , "MJ" ];'
   write(ud,'(a)')'energy.conversionTable    = [ unit,  kilo,  mega];'
   write(ud,'(a)')'energy.conversionOperator = "*";'
   write(ud,'(a)')
   write(ud,'(a)')'energy.choosenUnit = "J";'
   write(ud,'(a)')

   write(ud,'(a)')'//'
   write(ud,'(a)')'//- For force:'
   write(ud,'(a)')'//'
   write(ud,'(a)')'force.units              = ["N"  , "kN" ];'
   write(ud,'(a)')'force.conversionTable    = [ unit,  kilo];'
   write(ud,'(a)')'force.conversionOperator = "*";'
   write(ud,'(a)')
   write(ud,'(a)')'force.choosenUnit = "N";'
   write(ud,'(a)')
   
   write(ud,'(a)')'//'
   write(ud,'(a)')'//- For mass density:'
   write(ud,'(a)')'//'
   write(ud,'(a)')'mass_density.units              = ["kg/m3", "g/cm3"];'
   write(ud,'(a)')'mass_density.conversionTable    = [ unit  ,  milli ];'
   write(ud,'(a)')'mass_density.conversionOperator = "*";'
   write(ud,'(a)')
   write(ud,'(a)')'mass_density.choosenUnit = "kg/m3";'
   write(ud,'(a)')

   write(ud,'(a)')'//'
   write(ud,'(a)')'//- For power:'
   write(ud,'(a)')'//'
   write(ud,'(a)')'power.units              = ["W"  , "mW"  , "kW" , "MW" , "GW" ];'
   write(ud,'(a)')'power.conversionTable    = [ unit, milli,  kilo,  mega,  giga ];'
   write(ud,'(a)')'power.conversionOperator = "*";'
   write(ud,'(a)')
   write(ud,'(a)')'power.choosenUnit = "W";'
   write(ud,'(a)')
   
   write(ud,'(a)')'//'
   write(ud,'(a)')'//- For pressure:'
   write(ud,'(a)')'//'
   write(ud,'(a)')'pressure.units              = ["Pa" , "kPa", "bar", "MPa", "kbar", "GPa"];'
   write(ud,'(a)')'pressure.conversionTable    = [ unit,  kilo,  1e5 ,  mega,  1e8  ,  giga];'
   write(ud,'(a)')'pressure.conversionOperator = "*";'
   write(ud,'(a)')
   write(ud,'(a)')'pressure.choosenUnit = "GPa";'
   write(ud,'(a)')

   write(ud,'(a)')'//'
   write(ud,'(a)')'//- For strain:'
   write(ud,'(a)')'//'
   write(ud,'(a)')'strain.units              = ["-"  , "%"   ];'
   write(ud,'(a)')'strain.conversionTable    = [ unit,  hecto];'
   write(ud,'(a)')'strain.conversionOperator = "*";'
   write(ud,'(a)')
   write(ud,'(a)')'strain.choosenUnit = "-";'
   write(ud,'(a)')
   
   write(ud,'(a)')'//'
   write(ud,'(a)')'//- For strain rate:'
   write(ud,'(a)')'//'
   write(ud,'(a)')'strain_rate.units              = ["1/s", "1/min" , "1/h"  , "1/a"  , "1/Ma" ];'
   write(ud,'(a)')'strain_rate.conversionTable    = [ unit,  1/minut,  1/hour,  1/year, 1/Myear];'
   write(ud,'(a)')'strain_rate.conversionOperator = "*";'
   write(ud,'(a)')
   write(ud,'(a)')'strain_rate.choosenUnit = "1/s";'
   write(ud,'(a)')

   write(ud,'(a)')'//'
   write(ud,'(a)')'//- For stress:'
   write(ud,'(a)')'//'
   write(ud,'(a)')'stress.units              = ["Pa" , "kPa", "bar", "MPa", "kbar", "GPa"];'
   write(ud,'(a)')'stress.conversionTable    = [ unit,  kilo,  1e5 ,  mega,  1e8  ,  giga];'
   write(ud,'(a)')'stress.conversionOperator = "*";'
   write(ud,'(a)')
   write(ud,'(a)')'stress.choosenUnit = "MPa";'
   write(ud,'(a)')

   write(ud,'(a)')'//'
   write(ud,'(a)')'//- For temperature:'
   write(ud,'(a)')'//'
   write(ud,'(a)')'temperature.units              = ["K" , "C"     ];'
   write(ud,'(a)')'temperature.conversionTable    = [ 0.0,  273.15 ];'
   write(ud,'(a)')'temperature.conversionOperator = "+";'
   write(ud,'(a)')
   write(ud,'(a)')'temperature.choosenUnit = "K";'
   write(ud,'(a)')
   
   write(ud,'(a)')'//'
   write(ud,'(a)')'//- For thermal flux:'
   write(ud,'(a)')'//'
   write(ud,'(a)')'thermal_flux.units              = ["W/m2", "mW/m2"];'
   write(ud,'(a)')'thermal_flux.conversionTable    = [ unit ,  milli ];'
   write(ud,'(a)')'thermal_flux.conversionOperator = "*";'
   write(ud,'(a)')
   write(ud,'(a)')'thermal_flux.choosenUnit = "mW/m2";'
   write(ud,'(a)')
                           
   write(ud,'(a)')'//'
   write(ud,'(a)')'//- For time:'
   write(ud,'(a)')'//'
   write(ud,'(a)')'time.units              = ["s"  ,"min" , "h"  , "day", "a", "Ma"  ];'
   write(ud,'(a)')'time.conversionTable    = [ unit, minut,  hour,  day , year, Myear];'
   write(ud,'(a)')'time.conversionOperator = "*";'
   write(ud,'(a)')
   write(ud,'(a)')'time.choosenUnit = "Ma";'
   write(ud,'(a)')

   write(ud,'(a)')'//'
   write(ud,'(a)')'//- For velocities:'
   write(ud,'(a)')'//'
   write(ud,'(a)')'velocity.units              = ["m/s", "km/h"    , "cm/a"     , "mm/a"     ];'
   write(ud,'(a)')'velocity.conversionTable    = [ unit,  kilo/hour,  centi/year,  milli/year];'
   write(ud,'(a)')'velocity.conversionOperator = "*";'
   write(ud,'(a)')
   write(ud,'(a)')'velocity.choosenUnit = "cm/a";'
   write(ud,'(a)')   

   write(ud,'(a)')' //'
   write(ud,'(a)')' //- For viscosity:'
   write(ud,'(a)')' //'
   write(ud,'(a)')' viscosity.units              = ["Pa.s"];'
   write(ud,'(a)')' viscosity.conversionTable    = [ unit];'
   write(ud,'(a)')' viscosity.conversionOperator = "*";'
   write(ud,'(a)')
   write(ud,'(a)')' viscosity.choosenUnit = "Pa.s";'
   write(ud,'(a)')

     
   write(ud,'(a)')   
   write(ud,'(a)')'///////////////////////////////////////////////////////////////////////////'
   write(ud,'(a)')'//                Choice of invariants to be computed                    //'
   write(ud,'(a)')'///////////////////////////////////////////////////////////////////////////'
   write(ud,'(a)')
   write(ud,'(a)')'// Specify'
   write(ud,'(a)')'// - the type of invariant you want to compute (see (a)),'
   write(ud,'(a)')'// - the name of the variable (see (b)) to which you want to apply it,'
   write(ud,'(a)')'// - the output name of the result (the one that will appear in the vtu files).'
   write(ud,'(a)')'// To do this, use a 3-column string array named "invariants" (see (c) for the'
   write(ud,'(a)')'// syntax) as in the following example (you can add lines or remove any line):'
   write(ud,'(a)')
   write(ud,'(a)')'//               invariant type ,  variable name         ,  ouput name'
   write(ud,'(a)')' invariants = [ ...'
   write(ud,'(a)') &
   '                "isoPart"    , "contraintes"         , "mean_stress"     ; ...'
   write(ud,'(a)') &
   '                "equivStress", "contraintes"         , "Eqv(stress)"     ; ...'
   write(ud,'(a)') &
   '                "equivStrain", "deformations"        , "Eqv(strain)"     ; ...'
   write(ud,'(a)') &
   '                "equivStrain", "taux_de_deformations", "Eqv(strainRate)" ; ...'
   write(ud,'(a)') &
   '                "volPart"    , "deformations"        , "vol_strain"      ; ...'
   write(ud,'(a)') &
   '                "volPart"    , "taux_de_deformations", "vol_strainRate"    ...'
   write(ud,'(a)')'              ] ;'
   write(ud,'(a)')
   write(ud,'(a)')'// Notes:' 
   write(ud,'(a)')'// (a): predefined invariants are'
   write(ud,'(a)') &
   '//      - equivStress = Von Mises equivalent stress = sqrt{3J2(x)}   = sqrt{(3xd:xd/2}'
   write(ud,'(a)') &
   '//      - equivStrain = Von Mises equivalent strain = sqrt{4J2(x)/3} = sqrt{2xd:xd/3}'
   write(ud,'(a)')'//      - isoPart = trace(x)/3'
   write(ud,'(a)')'//      - volPart = trace(x)'
   write(ud,'(a)')'//      - sqrtI2 = sqrt(I2(x))'
   write(ud,'(a)')'//      - sqrtJ2 = sqrt(J2(x))'
   write(ud,'(a)') &
   '//      where  I2(x) = x:x/2,  J2(x) = xd:xd/2,   xd = dev(x),   trace(x) = x : I'
   write(ud,'(a)')
   write(ud,'(a)') &
   '// (b): if this variable is not found in your fem output file or if this variable'
   write(ud,'(a)') &
   '//      does not correspond to a symmetric second ordre tensor (6 components) the'
   write(ud,'(a)') &
   '//      corresponding invariant calculation will simply be ignored.'
   write(ud,'(a)')
   write(ud,'(a)') &
   '// (c): use comma (,) for column delimiter and semicolon (;) for line delimiter.'   
   write(ud,'(a)')
   
   write(ud,'(a)')
   write(ud,'(a)')'///////////////////////////////////////////////////////////////////////////'
   write(ud,'(a)')'//        Choice of eigenvalues and eigenvectors to be computed          //'
   write(ud,'(a)')'///////////////////////////////////////////////////////////////////////////'
   write(ud,'(a)')
   write(ud,'(a)')'// Specify'
   write(ud,'(a)')'// - if you want to calculate the eigen-elements of the whole tensor or of'
   write(ud,'(a)')'//   its deviatoric part (see (a)),'
   write(ud,'(a)')'// - the name of the corresponding variable (see (b)),'
   write(ud,'(a)')'// - the output name of the result (the one that will appear in the vtu files).'
   write(ud,'(a)')'// To do this, use a 3-column string array named "eigenvalues" (see (c) for the'
   write(ud,'(a)')'// syntax) as in the following example:'
   write(ud,'(a)')
   write(ud,'(a)')' eigenvalues = [ ...'
   write(ud,'(a)')'//                 "whole"    , "contraintes" , "sigma"    ; ...'
   write(ud,'(a)')'//                 "deviator" , "contraintes" , "devsigma"   ...'
   write(ud,'(a)')'               ] ;'
   write(ud,'(a)')
   write(ud,'(a)')'// Notes: '
   write(ud,'(a)')'// (a): choose among "whole" and "deviator". If you are not interested in any '
   write(ud,'(a)')'//      eigen-element leave commented the above lines (you will save time)' 
   write(ud,'(a)')
   write(ud,'(a)') &
   '// (b): if this variable is not found in your fem output file or if this variable'
   write(ud,'(a)') &
   '//      does not correspond to a symmetric second ordre tensor (6 components) the'
   write(ud,'(a)') &
   '//      corresponding eigen-element calculation will simply be ignored.'
   write(ud,'(a)')
   write(ud,'(a)') &
   '// (c): use comma (,) for column delimiter and semicolon (;) for line delimiter.'   
   write(ud,'(a)')

   write(ud,'(a)')
   write(ud,'(a)')'///////////////////////////////////////////////////////////////////////////'
   write(ud,'(a)')'//          Choice of the prefix/postfix of the input file               //'
   write(ud,'(a)')'///////////////////////////////////////////////////////////////////////////'
   write(ud,'(a)')
   write(ud,'(a)')'// If your input files always carry the same prefix or postfix you can set'
   write(ud,'(a)')'// the "wildcard" variable below to avoid having to type this extension and'
   write(ud,'(a)')'// let ptovtu add it for you.'
   write(ud,'(a)')'// Simply give in this string variable the prefix (followed by *) or the'
   write(ud,'(a)')'// postfix (preceded by *) or keep it empty when your files do not have'
   write(ud,'(a)')'// particular extension:'
   write(ud,'(a)')''
   write(ud,'(a)')' wildcard = " ";'
   write(ud,'(a)')'// wilcard = "p*";  // <-- my files start with  a "p"'
   write(ud,'(a)')'// wildcard = "*.out;" // <-- my files end with the extension ".out"'
   write(ud,'(a)')
   
   close(ud)
      
   END SUBROUTINE utilptovtu_createUserDefaultFile
   

!=============================================================================================   
   SUBROUTINE utilptovtu_end
!============================================================================================= 
   use pk2mod_m, only: err_ncall, i2a => util_intToChar
!---------------------------------------------------------------------------------------------
!  Displays an end message, print error/warning if any
!---------------------------------------------------------------------------------- RH 06/22 - 

!- local variables: --------------------------------------------------------------------------  
   integer :: n 
!---------------------------------------------------------------------------------------------
   
!
!- Print error messages if any and the number of warnings 
!
   if ( stat <= IZERO ) then
      call stat%addMsg ( msg = 'Normal termination ', before =.true. ) 
      n = err_ncall(2)
      if ( n == 1 ) then
         call stat%addMsg ( newline = .true., &
                                msg = 'with 1 warning (see message above)' )
      else if ( n > 1 ) then
         call stat%addMsg ( newline = .true., &
                                msg = 'with ' // i2a(n) // ' warnings (see messages above)' )
      end if
      if ( stat == EOF ) stat%has_loc = .false.
      call stat%display ( title = '--> ptovtu info:', trace = OFF )
   else
      call stat%display ( title = '--> ptovtu info:', trace = ON  )
   end if
!
!- Close the pfile:
!
   call pfile%close ()
   
   END SUBROUTINE utilptovtu_end
   
                  
!=============================================================================================
   FUNCTION utilptovtu_readError ( here, line, rec ) result ( quit )
!=============================================================================================
   integer  (Ikind),           intent(in) :: line
   character(len=*),           intent(in) :: here
   character(len=*), optional, intent(in) :: rec
   logical                                :: quit 
!---------------------------------------------------------------------------------------------
!
!---------------------------------------------------------------------------------------------

   if ( iostat /= IZERO ) then
      if ( iostat > IZERO ) call utilptovtu_error &
                   ( msg = 'Read error', err = UERROR, where = here, rec = rec, line = line ) 
      if ( iostat < IZERO ) quit = .true.  
   else
      quit = .false.
   end if
   
   END FUNCTION utilptovtu_readError
   
           
!=============================================================================================
   SUBROUTINE utilptovtu_error ( msg, err, where, rec, line )
!=============================================================================================
   character(len=*),           intent(in) :: msg
   integer  (Ikind),           intent(in) :: err
   character(len=*), optional, intent(in) :: where, rec
   integer  (Ikind), optional, intent(in) :: line
!---------------------------------------------------------------------------------------------
!
!---------------------------------------------------------------------------------------------

!- local variables: --------------------------------------------------------------------------   
   character(len=:), allocatable :: message
!---------------------------------------------------------------------------------------------

   message = msg
   
   if (present(line)) then
      message = message // NL // "--> (at record #" // util_intToChar(line) // &
                " of the file '"//trim(filein)//"')"
   end if

   if (present(rec)) message = message // NL // &
                               '--> Where: in expression "' // trim(rec) // '"'
                                   
   stat = err_t ( stat = err, where = where, msg = message )
   
   if (err == WARNING) &
      call stat%display( title = '--> ptovtu info:', delete = .true.)
 
   END SUBROUTINE utilptovtu_error


!=============================================================================================   
   PURE SUBROUTINE utilptovtu_eigval3x3 (a, w)
!=============================================================================================   
   real(Rkind), intent(in    ) :: a(3, 3) !> The symmetric input matrix   
   real(Rkind), intent(   out) :: w(3)    !> Contains eigenvalues on exit
!---------------------------------------------------------------------------------------------
! This is free and unencumbered software released into the public domain.
!
! Anyone is free to copy, modify, publish, use, compile, sell, or
! distribute this software, either in source code form or as a compiled
! binary, for any purpose, commercial or non-commercial, and by any
! means.
!
! In jurisdictions that recognize copyright laws, the author or authors
! of this software dedicate any and all copyright interest in the
! software to the public domain. We make this dedication for the benefit
! of the public at large and to the detriment of our heirs and
! successors. We intend this dedication to be an overt act of
! relinquishment in perpetuity of all present and future rights to this
! software under copyright law.
!
! THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
! EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
! MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
! IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
! OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
! ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
! OTHER DEALINGS IN THE SOFTWARE.
!
! For more information, please refer to <https://unlicense.org>
!
!  Analytical implementation of the 3x3 eigenproblem and its solution
!  Calculates eigenvalues based on the trigonometric solution of A = pB + qI
!
!  Author: Sebastian Ehlert
!---------------------------------------------------------------------------------------------

!- local variables: --------------------------------------------------------------------------
   real(Rkind), parameter :: twothirdpi = 8.0_Rkind * atan(1.0_Rkind) / 3.0_Rkind
   real(Rkind)            :: q, p, r
!---------------------------------------------------------------------------------------------

   r = a(1, 2) * a(1, 2) + a(1, 3) * a(1, 3) + a(2, 3) * a(2, 3)
   q = (a(1, 1) + a(2, 2) + a(3, 3)) / 3.0_Rkind
   w(1) = a(1, 1) - q
   w(2) = a(2, 2) - q
   w(3) = a(3, 3) - q
   p = sqrt((w(1) * w(1) + w(2) * w(2) + w(3) * w(3) + 2*r) / 6.0_Rkind)
   r = (w(1) * (w(2) * w(3) - a(2, 3) * a(2, 3)) &
      & - a(1, 2) * (a(1, 2) * w(3) - a(2, 3) * a(1, 3)) &
      & + a(1, 3) * (a(1, 2) * a(2, 3) - w(2) * a(1, 3))) / (p*p*p) * 0.5_Rkind

   if (r <= -1.0_Rkind) then
      r = 0.5_Rkind * twothirdpi
   else if (r >= 1.0_Rkind) then
      r = 0.0_Rkind
   else
      r = acos(r) / 3.0_Rkind
   end if

   w(3) = q + 2 * p * cos(r)
   w(1) = q + 2 * p * cos(r + twothirdpi)
   w(2) = 3 * q - w(1) - w(3)

   END SUBROUTINE utilptovtu_eigval3x3


!=============================================================================================   
   PURE SUBROUTINE utilptovtu_eigvec3x3 ( a, w, q )
!=============================================================================================   
   real(Rkind), intent(in out) :: a(3,3) !> The symmetric input matrix, destroyed while solving
   real(Rkind), intent(   out) :: w(3)   !> Contains eigenvalues on exit
   real(Rkind), intent(   out) :: q(3,3) !> Contains eigenvectors on exit
!---------------------------------------------------------------------------------------------
!  Calculates eigenvector using an analytical method based on vector cross products.
!
!  Author: Sebastian Ehlert
!---------------------------------------------------------------------------------------------

!- local variables: --------------------------------------------------------------------------
   real   (Rkind), parameter :: eps = epsilon(1.0_Rkind)
   real   (Rkind)            :: norm, n1, n2, n3, precon
   integer                   :: i
!---------------------------------------------------------------------------------------------
   

   w(1) = max(abs(a(1, 1)), abs(a(1, 2)))
   w(2) = max(abs(a(1, 3)), abs(a(2, 2)))
   w(3) = max(abs(a(2, 3)), abs(a(3, 3)))
   precon = max(w(1), max(w(2), w(3)))

   ! null matrix
   if (precon < eps) then
      w(1) = 0.0_Rkind
      w(2) = 0.0_Rkind
      w(3) = 0.0_Rkind
      q(1, 1) = 1.0_Rkind
      q(2, 2) = 1.0_Rkind
      q(3, 3) = 1.0_Rkind
      q(1, 2) = 0.0_Rkind
      q(1, 3) = 0.0_Rkind
      q(2, 3) = 0.0_Rkind
      q(2, 1) = 0.0_Rkind
      q(3, 1) = 0.0_Rkind
      q(3, 2) = 0.0_Rkind
      return
   end if

   norm = 1.0_Rkind / precon

   a(1, 1) = a(1, 1) * norm
   a(1, 2) = a(1, 2) * norm
   a(2, 2) = a(2, 2) * norm
   a(1, 3) = a(1, 3) * norm
   a(2, 3) = a(2, 3) * norm
   a(3, 3) = a(3, 3) * norm

   ! Calculate eigenvalues
   call utilptovtu_eigval3x3 ( a, w )

   ! Compute first eigenvector
   a(1, 1) = a(1, 1) - w(1)
   a(2, 2) = a(2, 2) - w(1)
   a(3, 3) = a(3, 3) - w(1)

   q(1, 1) = a(1, 2) * a(2, 3) - a(1, 3) * a(2, 2)
   q(2, 1) = a(1, 3) * a(1, 2) - a(1, 1) * a(2, 3)
   q(3, 1) = a(1, 1) * a(2, 2) - a(1, 2) * a(1, 2)
   q(1, 2) = a(1, 2) * a(3, 3) - a(1, 3) * a(2, 3)
   q(2, 2) = a(1, 3) * a(1, 3) - a(1, 1) * a(3, 3)
   q(3, 2) = a(1, 1) * a(2, 3) - a(1, 2) * a(1, 3)
   q(1, 3) = a(2, 2) * a(3, 3) - a(2, 3) * a(2, 3)
   q(2, 3) = a(2, 3) * a(1, 3) - a(1, 2) * a(3, 3)
   q(3, 3) = a(1, 2) * a(2, 3) - a(2, 2) * a(1, 3)
   n1 = q(1, 1) * q(1, 1) + q(2, 1) * q(2, 1) + q(3, 1) * q(3, 1)
   n2 = q(1, 2) * q(1, 2) + q(2, 2) * q(2, 2) + q(3, 2) * q(3, 2)
   n3 = q(1, 3) * q(1, 3) + q(2, 3) * q(2, 3) + q(3, 3) * q(3, 3)

   norm = n1
   i = 1
   if (n2 > norm) then
      i = 2
      norm = n1
   end if
   if (n3 > norm) then
      i = 3
   end if

   if (i == 1) then
      norm = sqrt(1.0_Rkind / n1)
      q(1, 1) = q(1, 1) * norm
      q(2, 1) = q(2, 1) * norm
      q(3, 1) = q(3, 1) * norm
   else if (i == 2) then
      norm = sqrt(1.0_Rkind / n2)
      q(1, 1) = q(1, 2) * norm
      q(2, 1) = q(2, 2) * norm
      q(3, 1) = q(3, 2) * norm
   else
      norm = sqrt(1.0_Rkind / n3)
      q(1, 1) = q(1, 3) * norm
      q(2, 1) = q(2, 3) * norm
      q(3, 1) = q(3, 3) * norm
   end if

   ! Robustly compute a right-hand orthonormal set (ev1, u, v)
   if (abs(q(1, 1)) > abs(q(2, 1))) then
      norm = sqrt(1.0_Rkind / (q(1, 1) * q(1, 1) + q(3, 1) * q(3, 1)))
      q(1, 2) = -q(3, 1) * norm
      q(2, 2) = 0.0_Rkind
      q(3, 2) = +q(1, 1) * norm
   else
      norm = sqrt(1.0_Rkind / (q(2, 1) * q(2, 1) + q(3, 1) * q(3, 1)))
      q(1, 2) = 0.0_Rkind
      q(2, 2) = +q(3, 1) * norm
      q(3, 2) = -q(2, 1) * norm
   end if
   q(1, 3) = q(2, 1) * q(3, 2) - q(3, 1) * q(2, 2)
   q(2, 3) = q(3, 1) * q(1, 2) - q(1, 1) * q(3, 2)
   q(3, 3) = q(1, 1) * q(2, 2) - q(2, 1) * q(1, 2)

   ! Reset A
   a(1, 1) = a(1, 1) + w(1)
   a(2, 2) = a(2, 2) + w(1)
   a(3, 3) = a(3, 3) + w(1)

   ! A*U
   n1 = a(1, 1) * q(1, 2) + a(1, 2) * q(2, 2) + a(1, 3) * q(3, 2)
   n2 = a(1, 2) * q(1, 2) + a(2, 2) * q(2, 2) + a(2, 3) * q(3, 2)
   n3 = a(1, 3) * q(1, 2) + a(2, 3) * q(2, 2) + a(3, 3) * q(3, 2)

   ! A*V, note out of order computation
   a(3, 3) = a(1, 3) * q(1, 3) + a(2, 3) * q(2, 3) + a(3, 3) * q(3, 3)
   a(1, 3) = a(1, 1) * q(1, 3) + a(1, 2) * q(2, 3) + a(1, 3) * q(3, 3)
   a(2, 3) = a(1, 2) * q(1, 3) + a(2, 2) * q(2, 3) + a(2, 3) * q(3, 3)

   ! UT*(A*U) - l2*E
   n1 = q(1, 2) * n1 + q(2, 2) * n2 + q(3, 2) * n3 - w(2)
   ! UT*(A*V)
   n2 = q(1, 2) * a(1, 3) + q(2, 2) * a(2, 3) + q(3, 2) * a(3, 3)
   ! VT*(A*V) - l2*E
   n3 = q(1, 3) * a(1, 3) + q(2, 3) * a(2, 3) + q(3, 3) * a(3, 3) - w(2)

   if (abs(n1) >= abs(n3)) then
      norm = max(abs(n1), abs(n2))
      if (norm > eps) then
         if (abs(n1) >= abs(n2)) then
            n2 = n2 / n1
            n1 = sqrt(1.0_Rkind / (1.0_Rkind + n2 * n2))
            n2 = n2 * n1
         else
            n1 = n1 / n2
            n2 = sqrt(1.0_Rkind / (1.0_Rkind + n1 * n1))
            n1 = n1 * n2
         end if
         q(1, 2) = n2 * q(1, 2) - n1 * q(1, 3)
         q(2, 2) = n2 * q(2, 2) - n1 * q(2, 3)
         q(3, 2) = n2 * q(3, 2) - n1 * q(3, 3)
      end if
   else
      norm = max(abs(n3), abs(n2))
      if (norm > eps) then
         if (abs(n3) >= abs(n2)) then
            n2 = n2 / n3
            n3 = sqrt(1.0_Rkind / (1.0_Rkind + n2 * n2))
            n2 = n2 * n3
         else
            n3 = n3 / n2
            n2 = sqrt(1.0_Rkind / (1.0_Rkind + n3 * n3))
            n3 = n3 * n2
         end if
         q(1, 2) = n3 * q(1, 2) - n2 * q(1, 3)
         q(2, 2) = n3 * q(2, 2) - n2 * q(2, 3)
         q(3, 2) = n3 * q(3, 2) - n2 * q(3, 3)
      end if
   end if

   ! Calculate third eigenvector from cross product
   q(1, 3) = q(2, 1) * q(3, 2) - q(3, 1) * q(2, 2)
   q(2, 3) = q(3, 1) * q(1, 2) - q(1, 1) * q(3, 2)
   q(3, 3) = q(1, 1) * q(2, 2) - q(2, 1) * q(1, 2)

   w(1) = w(1) * precon
   w(2) = w(2) * precon
   w(3) = w(3) * precon

   END SUBROUTINE utilptovtu_eigvec3x3


!=============================================================================================
   SUBROUTINE utilptovtu_eigenMatSym3 ( mat, sig1, sig2, sig3, dir1, dir2, dir3 )
!=============================================================================================
   real   (Rkind), intent(in out) :: mat(3,3)
   real   (Rkind), intent(   out) :: sig1, sig2, sig3, dir1(3), dir2(3), dir3(3)
!---------------------------------------------------------------------------------------------  
!  Computes the eigen-elements of a symmetric 3x3 matrix. Sorts them according to the 
!  ascending order of the eigenvalues. 
!
!  Called routine:
!  . utilptovtu_SYEVJ3 (alias dsyevJ3) by Joachim Kopp
!---------------------------------------------------------------------------------------------  

!- local variables: --------------------------------------------------------------------------
   integer(Ikind) :: i1, i2, i3, iv, next(3) = [2,3,1]
   real   (Rkind) :: valmin, valp(3), vecp(3,3)
!---------------------------------------------------------------------------------------------    

   call utilptovtu_SYEVJ3 ( mat, vecp, valp )  
!   
!- 1) find the smallest eigenvalue (index i1 in valp):
!
   i1 = 1; valmin = valp(1)
   do iv = 2, 3
      if (valp(iv) < valmin) then
         valmin = valp(iv) ; i1 = iv
      end if
   end do
!
!- 2) find i2 and i3 such that valp(i2) < valp(i3):
!
   i2 = next(i1); i3 = next(i2)
   if (valp(i3) < valp(i2)) then
      iv = i2 ; i2 = i3; i3 = iv
   end if
!
!- 3) return the eigenvalues in sig* and the corresponding eigenvectors in dir*:
!
   sig1 = valp(i1) ; dir1(:) = vecp(:,i1) 
   sig2 = valp(i2) ; dir2(:) = vecp(:,i2)
   sig3 = valp(i3) ; dir3(:) = vecp(:,i3) 

   END SUBROUTINE utilptovtu_eigenMatSym3   
   
   
!=============================================================================================
   SUBROUTINE utilptovtu_SYEVJ3 (A, Q, W)
!=============================================================================================
! ----------------------------------------------------------------------------
!  Numerical diagonalization of 3x3 matrcies
!  Copyright (C) 2006  Joachim Kopp
! ----------------------------------------------------------------------------
!  This library is free software; you can redistribute it and/or
!  modify it under the terms of the GNU Lesser General Public
!  License as published by the Free Software Foundation; either
!  version 2.1 of the License, or (at your option) any later version.
!
!  This library is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
!  Lesser General Public License for more details.
!
!  You should have received a copy of the GNU Lesser General Public
!  License along with this library; if not, write to the Free Software
!  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
!
!  Calculates the eigenvalues and normalized eigenvectors of a symmetric 3x3
!  matrix A using the Jacobi algorithm.
!  The upper triangular part of A is destroyed during the calculation,
!  the diagonal elements are read but not destroyed, and the lower
!  triangular elements are not referenced at all.
! ----------------------------------------------------------------------------
!  Parameters:
!   A: The symmetric input matrix
!   Q: Storage buffer for eigenvectors
!   W: Storage buffer for eigenvalues
! ----------------------------------------------------------------------------
!     .. Arguments ..
   real(Rkind) ::  a(3,3), q(3,3), w(3)

!     .. parameters ..
   integer    , parameter :: n = 3
   real(Rkind), parameter :: zero = 0.0_Rkind, twotenth = 0.2_Rkind, half = 0.5_Rkind, & 
                             one = 1.0_Rkind, hund = 100.0_Rkind
    
!     .. local variables ..
   real(Rkind) :: sd, so
   real(Rkind) :: s, c, t
   real(Rkind) :: g, h, z, theta
   real(Rkind) :: thresh
   integer     :: i, x, y, r

!     initialize q to the identitity matrix
!     --- this loop can be omitted if only the eigenvalues are desired ---
      
      sd = ZERO
      do 10 x = 1, n
        q(x,x) = ONE
        do 11, y = 1, x-1
          q(x, y) = ZERO
          q(y, x) = ZERO
          sd = max(sd,abs(a(x,y)))
   11   continue
   10 continue

!     initialize w to diag(a)
      do 20 x = 1, n
        w(x) = a(x, x)
   20 continue
   
      if (sd == ZERO) return

!     calculate sqr(tr(a))  
      sd = ZERO
      do 30 x = 1, n
        sd = sd + abs(w(x))
   30 continue
      sd = sd**2
 
!     main iteration loop
      do 40 i = 1, 50
!       test for convergence
        so = ZERO
        do 50 x = 1, n
          do 51 y = x+1, n
            so = so + abs(a(x, y))
   51     continue
   50   continue
        if (so .eq. ZERO) then
          return
        end if

        if (i .lt. 4) then
          thresh = TWOTENTH * so / n**2
        else
          thresh = ZERO
        end if

!       do sweep
        do 60 x = 1, n
          do 61 y = x+1, n
            g = HUND * ( abs(a(x, y)) )
            if ( i .gt. 4 .and. abs(w(x)) + g .eq. abs(w(x))       &
                          .and. abs(w(y)) + g .eq. abs(w(y)) ) then
              a(x, y) = ZERO
            else if (abs(a(x, y)) .gt. thresh) then
!             calculate jacobi transformation
              h = w(y) - w(x)
              if ( abs(h) + g .eq. abs(h) ) then
                t = a(x, y) / h
              else
                theta = HALF * h / a(x, y)
                if (theta .lt. ZERO) then
                  t = -ONE / (sqrt(ONE + theta**2) - theta)
                else
                  t = ONE / (sqrt(ONE + theta**2) + theta)
                end if
              end if

              c = ONE / sqrt( ONE + t**2 ) 
              s = t * c
              z = t * a(x, y)
              
!             apply jacobi transformation
              a(x, y) = ZERO
              w(x)    = w(x) - z
              w(y)    = w(y) + z
              do 70 r = 1, x-1
                t       = a(r, x)
                a(r, x) = c * t - s * a(r, y)
                a(r, y) = s * t + c * a(r, y)
   70         continue
              do 80, r = x+1, y-1
                t       = a(x, r)
                a(x, r) = c * t - s * a(r, y)
                a(r, y) = s * t + c * a(r, y)
   80         continue
              do 90, r = y+1, n
                t       = a(x, r)
                a(x, r) = c * t - s * a(y, r)
                a(y, r) = s * t + c * a(y, r)
   90         continue

!             update eigenvectors
!             --- this loop can be omitted if only the eigenvalues are desired ---
              do 100, r = 1, n
                t       = q(r, x)
                q(r, x) = c * t - s * q(r, y)
                q(r, y) = s * t + c * q(r, y)
  100         continue
            end if
   61     continue
   60   continue
   40 continue

      print *, "dsyevj3: no convergence."

   END SUBROUTINE utilptovtu_SYEVJ3 
      
      
END MODULE utilptovtu_m
