!---------------------------------------------------------------------------------------------
! ptovtu, version 2022.7.0
!---------------------------------------------------------------------------------------------
!
! Module name: 
!       conn_m
!
! Description: 
!       Define a DT for connectivity tables
! 
! Notes: 
!       
!
! Author(s): 
!        R. Hassani, Universite Côte d'Azur
!
! Start date: 
!        06/22
!
! Changes:
!        07/22 - 08/222
!---------------------------------------------------------------------------------------------

#include "error.fpp"

MODULE conn_m
   use file_m,            only: file_t 
   use constantsptovtu_m, only: Ikind, i32, i64, NLT, UERROR, IZERO 
   
   use pk2mod_m,            only: err_t 
   
   implicit none
   
   private
   public :: conn32_t, conn64_t, conn_vtkNumNodesInit
!
!- Define a DT for connectivity tables:
!   
   type, abstract :: conn_t
      ! number of geom. entities (cells, facets, edges, ...):
      integer  (Ikind) :: n = 0 
      ! size of the connectivity table (rank-1 array):
      integer  ( i64 ) :: usedSize = 0
   contains
      procedure(interf_allocConn  ), deferred, pass(self) :: alloc
      procedure(interf_readConnect), deferred, pass(self) :: readConnect   
      procedure(interf_readTypes  ), deferred, pass(self) :: readTypes  
      procedure(interf_readTypes  ), deferred, pass(self) :: readDomains  
      procedure(interf_computeIndx), deferred, pass(self) :: computeIndx  
   end type conn_t
   
   type, extends(conn_t) :: conn32_t
      integer(i32), allocatable :: connect (:), types (:), domains (:), indx (:)
   contains
      procedure, pass(self) :: alloc       => conn_allocConn32      
      procedure, pass(self) :: readConnect => conn_readConnect32   
      procedure, pass(self) :: readTypes   => conn_readTypes32      
      procedure, pass(self) :: readDomains => conn_readDomains32      
      procedure, pass(self) :: computeIndx => conn_computeIndx32      
   end type conn32_t
   
   type, extends(conn_t) :: conn64_t
      integer(i64), allocatable :: connect (:), types (:), domains (:), indx (:) 
   contains
      procedure, pass(self) :: alloc       => conn_allocConn64      
      procedure, pass(self) :: readConnect => conn_readConnect64      
      procedure, pass(self) :: readTypes   => conn_readTypes64   
      procedure, pass(self) :: readDomains => conn_readDomains64   
      procedure, pass(self) :: computeIndx => conn_computeIndx64      
   end type conn64_t   

   abstract interface
       subroutine interf_allocConn ( self, n, msize ) 
          import :: conn_t, Ikind, i64
          class    (conn_t),           intent(in out) :: self 
          integer  (Ikind ), optional, intent(in    ) :: n
          integer  ( i64  ), optional, intent(in    ) :: msize 
       end subroutine interf_allocConn  

       subroutine interf_readConnect ( self, file, stat, what ) 
         import :: conn_t, file_t, err_t
         class    (conn_t),           intent(in out) :: self 
         class    (file_t),           intent(in out) :: file 
         type     (err_t ),           intent(in out) :: stat
         character(len=* ), optional, intent(in    ) :: what
      end subroutine interf_readConnect   

       subroutine interf_readTypes ( self, file, nrec, stat, what ) 
         import :: conn_t, file_t, Ikind,  err_t
         class    (conn_t),           intent(in out) :: self 
         class    (file_t),           intent(in out) :: file 
         integer  (Ikind ),           intent(in    ) :: nrec
         type     (err_t ),           intent(in out) :: stat
         character(len=* ), optional, intent(in    ) :: what
      end subroutine interf_readTypes   

       subroutine interf_computeIndx ( self, stat, what ) 
         import :: conn_t, err_t
         class    (conn_t),           intent(in out) :: self 
         type     (err_t ),           intent(in out) :: stat
         character(len=* ), optional, intent(in    ) :: what
      end subroutine interf_computeIndx   
      
   end interface      
          
   integer(Ikind) :: vtkNumNodes(25) = IZERO
       
CONTAINS

!=============================================================================================
   SUBROUTINE conn_allocConn32 ( self, n, msize )
!=============================================================================================
   class    (conn32_t),           intent(in out) :: self 
   integer  (Ikind   ), optional, intent(in    ) :: n
   integer  (  i64   ), optional, intent(in    ) :: msize 
!--------------------------------------------------------------------------------------------- 
!
!---------------------------------------------------------------------------------- RH 06/22 - 

   if ( present(n) ) then
      ! if %indx, %types or %domains are not big enough, resize them:
      if ( allocated(self%indx) ) then
         if ( size(self%indx) < n ) deallocate( self%indx )
      end if
      if ( .not. allocated(self%indx) ) allocate( self%indx(n) )
      
      if ( allocated(self%types) ) then
         if ( size(self%types) < n ) deallocate( self%types )
      end if
      if ( .not. allocated(self%types) ) allocate( self%types(n) )   

      if ( allocated(self%domains) ) then
         if ( size(self%domains) < n ) deallocate( self%domains )
      end if
      if ( .not. allocated(self%domains) ) allocate( self%domains(n) )   
         
      self%n = n
   end if

   if ( present(msize) ) then
   
      ! if %connect is not big enough, resize it:
      if ( allocated(self%connect) ) then
         if ( size(self%connect) < msize ) deallocate( self%connect )
      end if
      if ( .not. allocated(self%connect) ) allocate( self%connect(msize) )
      
      ! save the used size:
       self%usedSize = msize   
   end if 
   
   END SUBROUTINE conn_allocConn32

!=============================================================================================
   SUBROUTINE conn_allocConn64 ( self, n, msize )
!=============================================================================================
   class    (conn64_t),           intent(in out) :: self 
   integer  (Ikind   ), optional, intent(in    ) :: n
   integer  (  i64   ), optional, intent(in    ) :: msize 
!--------------------------------------------------------------------------------------------- 
!
!---------------------------------------------------------------------------------- RH 06/22 - 

   if ( present(n) ) then
      ! if %indx, %types or %domains are not big enough, resize them:
      if ( allocated(self%indx) ) then
         if ( size(self%indx) < n ) deallocate( self%indx )
      end if
      if ( .not. allocated(self%indx) ) allocate( self%indx(n) )
      
      if ( allocated(self%types) ) then
         if ( size(self%types) < n ) deallocate( self%types )
      end if
      if ( .not. allocated(self%types) ) allocate( self%types(n) )   

      if ( allocated(self%domains) ) then
         if ( size(self%domains) < n ) deallocate( self%domains )
      end if
      if ( .not. allocated(self%domains) ) allocate( self%domains(n) )   
         
      self%n = n
   end if

   if ( present(msize) ) then
   
      ! if %connect is not big enough, resize it:
      if ( allocated(self%connect) ) then
         if ( size(self%connect) < msize ) deallocate( self%connect )
      end if
      if ( .not. allocated(self%connect) ) allocate( self%connect(msize) )
      
      ! save the used size:
       self%usedSize = msize   
   end if 
   
   END SUBROUTINE conn_allocConn64   
   
         
!=============================================================================================
   SUBROUTINE conn_readConnect32 ( self, file, stat, what )
!=============================================================================================
   class    (conn32_t),           intent(in out) :: self 
   class    (file_t  ),           intent(in out) :: file 
   type     (err_t   ),           intent(in out) :: stat
   character(len=*   ), optional, intent(in    ) :: what
!--------------------------------------------------------------------------------------------- 
!
!---------------------------------------------------------------------------------- RH 06/22 - 

   call file%read ( self%connect(1:self%usedSize), stat, what )
   error_TraceNreturn(stat/=IZERO, 'conn_readConnect32', stat)
   
   END SUBROUTINE conn_readConnect32      

!=============================================================================================
   SUBROUTINE conn_readConnect64 ( self, file, stat, what )
!=============================================================================================
   class    (conn64_t),           intent(in out) :: self 
   class    (file_t  ),           intent(in out) :: file 
   type     (err_t   ),           intent(in out) :: stat
   character(len=*   ), optional, intent(in    ) :: what
!--------------------------------------------------------------------------------------------- 
!
!---------------------------------------------------------------------------------- RH 06/22 - 

   call file%read ( self%connect(1:self%usedSize), stat, what )
   error_TraceNreturn(stat/=IZERO, 'conn_readConnect64', stat)
   
   END SUBROUTINE conn_readConnect64  


!=============================================================================================
   SUBROUTINE conn_readDomains32 ( self, file, nrec, stat, what )
!=============================================================================================
   class    (conn32_t),           intent(in out) :: self 
   class    (file_t  ),           intent(in out) :: file 
   integer  (Ikind   ),           intent(in    ) :: nrec
   type     (err_t   ),           intent(in out) :: stat
   character(len=*   ), optional, intent(in    ) :: what
!--------------------------------------------------------------------------------------------- 
!
!---------------------------------------------------------------------------------- RH 06/22 - 

!- local variables ---------------------------------------------------------------------------
   character(len=*), parameter :: HERE = 'conn_readDomains32'
   integer  ( i32 )            :: id
!--------------------------------------------------------------------------------------------- 
   
   if ( nrec == 1 ) then
      call file%read ( id, stat, what )
      self%domains(1:self%n) = id
   else if ( nrec == self%n ) then
      call file%read ( self%domains(1:nrec), stat, what )
   else
      stat = err_t ( stat = UERROR, where = HERE, msg = '('// what //') ' //    &
                     'Inconsistency between the size of the domain Id list ' // &  
                     NLT // '(self%domains) and the number of records to read.' )
      return
   end if
   
   error_TraceNreturn(stat/=IZERO, HERE, stat)
   
   END SUBROUTINE conn_readDomains32         

!=============================================================================================
   SUBROUTINE conn_readDomains64 ( self, file, nrec, stat, what )
!=============================================================================================
   class    (conn64_t),           intent(in out) :: self 
   class    (file_t  ),           intent(in out) :: file 
   integer  (Ikind   ),           intent(in    ) :: nrec
   type     (err_t   ),           intent(in out) :: stat
   character(len=*   ), optional, intent(in    ) :: what
!--------------------------------------------------------------------------------------------- 
!
!---------------------------------------------------------------------------------- RH 06/22 - 

!- local variables ---------------------------------------------------------------------------
   character(len=*), parameter :: HERE = 'conn_readDomains64'
   integer  ( i64 )            :: id
!--------------------------------------------------------------------------------------------- 
   
   if ( nrec == 1 ) then
      call file%read ( id, stat, what )
      self%domains(1:self%n) = id
   else if ( nrec == self%n ) then
      call file%read ( self%domains(1:nrec), stat, what )
   else
      stat = err_t ( stat = UERROR, where = HERE, msg = 'Read of '// what //        &
                     ': Inconsistency between the size of the domain Id list ' //   & 
                     '(self%domains)'// NLT // 'and the number of records to read.' )
      return
   end if
   
   error_TraceNreturn(stat/=IZERO, HERE, stat)
   
   END SUBROUTINE conn_readDomains64
   

!=============================================================================================
   SUBROUTINE conn_readTypes32 ( self, file, nrec, stat, what )
!=============================================================================================
   class    (conn32_t),           intent(in out) :: self 
   class    (file_t  ),           intent(in out) :: file 
   integer  (Ikind   ),           intent(in    ) :: nrec
   type     (err_t   ),           intent(in out) :: stat
   character(len=*   ), optional, intent(in    ) :: what
!--------------------------------------------------------------------------------------------- 
!
!---------------------------------------------------------------------------------- RH 06/22 - 

!- local variables ---------------------------------------------------------------------------
   character(len=*), parameter :: HERE = 'conn_readTypes32'
   integer  ( i32 )            :: id
!--------------------------------------------------------------------------------------------- 
   
   if ( nrec == 1 ) then
      call file%read ( id, stat, what )
      self%types(1:self%n) = id
   else if ( nrec == self%n ) then
      call file%read ( self%types(1:nrec), stat, what )
   else
      stat = err_t ( stat = UERROR, where = HERE,                                        &
                      msg = 'Inconsistency between the size of the types Id list ' //    & 
                            '(self%types)'// NLT // 'and the number of records to read.' )
      return
   end if
   
   error_TraceNreturn(stat/=IZERO, HERE, stat)
   
   END SUBROUTINE conn_readTypes32

!=============================================================================================
   SUBROUTINE conn_readTypes64 ( self, file, nrec, stat, what )
!=============================================================================================
   class    (conn64_t),           intent(in out) :: self 
   class    (file_t  ),           intent(in out) :: file 
   integer  (Ikind   ),           intent(in    ) :: nrec
   type     (err_t   ),           intent(in out) :: stat
   character(len=*   ), optional, intent(in    ) :: what
!--------------------------------------------------------------------------------------------- 
!
!---------------------------------------------------------------------------------- RH 06/22 - 

!- local variables ---------------------------------------------------------------------------
   character(len=*), parameter :: HERE = 'conn_readTypes64'
   integer  ( i64 )            :: id
!--------------------------------------------------------------------------------------------- 
   
   if ( nrec == 1 ) then
      call file%read ( id, stat, what )
      self%types(1:self%n) = id
   else if ( nrec == self%n ) then
      call file%read ( self%types(1:nrec), stat, what )
   else
      stat = err_t ( stat = UERROR, where = HERE,                                        &
                      msg = 'Inconsistency between the size of the types Id list ' //    & 
                            '(self%types)'// NLT // 'and the number of records to read.' )
      return
   end if
   
   error_TraceNreturn(stat/=IZERO, HERE, stat)
   
   END SUBROUTINE conn_readTypes64
   
                            
!=============================================================================================
   SUBROUTINE conn_computeIndx32 ( self, stat, what )
!=============================================================================================
   class    (conn32_t),           intent(in out) :: self 
   type     (err_t   ),           intent(in out) :: stat
   character(len=*   ), optional, intent(in    ) :: what
!--------------------------------------------------------------------------------------------- 
!
!---------------------------------------------------------------------------------- RH 06/22 - 

!- local variables ---------------------------------------------------------------------------
   character(len=*), parameter :: HERE = 'conn_computeIndx32'
   integer  (Ikind)            :: i, nNode
!--------------------------------------------------------------------------------------------- 
     
   self%indx(1) = vtkNumNodes(self%types(1))
   do i = 2, self%n
      nNode = vtkNumNodes(self%types(i))
      self%indx(i) = self%indx(i-1) + nNode
   end do
   
   if ( self%usedSize /= self%indx(self%n) ) &
      stat = err_t ( stat = UERROR, where = HERE,  msg = what // ' ' //   &
            'The given size of the connectivity table '// NLT //          &
            'is not consistent with that computed from the element types' )
   
   END SUBROUTINE conn_computeIndx32      


!=============================================================================================
   SUBROUTINE conn_computeIndx64 ( self, stat, what )
!=============================================================================================
   class    (conn64_t),           intent(in out) :: self 
   type     (err_t   ),           intent(in out) :: stat
   character(len=*   ), optional, intent(in    ) :: what   
!--------------------------------------------------------------------------------------------- 
!
!---------------------------------------------------------------------------------- RH 06/22 - 

!- local variables ---------------------------------------------------------------------------
   character(len=*), parameter :: HERE = 'conn_computeIndx64'
   integer  (Ikind)            :: i, nNode
!--------------------------------------------------------------------------------------------- 
     
   self%indx(1) = vtkNumNodes(self%types(1))
   do i = 2, self%n
      nNode = vtkNumNodes(self%types(i))
      self%indx(i) = self%indx(i-1) + nNode
   end do
   
   if ( self%usedSize /= self%indx(self%n) ) &
      stat = err_t ( stat = UERROR, where = HERE,  msg = what // ' ' //   &
            'The given size of the connectivity table '// NLT //          &
            'is not consistent with that computed from the element types' )
   
   END SUBROUTINE conn_computeIndx64
        

!=============================================================================================
   SUBROUTINE conn_vtkNumNodesInit
!=============================================================================================

!--------------------------------------------------------------------------------------------- 
!  Sets the number of nodes for each simplex vtk code.
!---------------------------------------------------------------------------------- RH 06/22 -

!- local variables: --------------------------------------------------------------------------  
   logical, save :: is_init = .false. 
!--------------------------------------------------------------------------------------------- 

   if ( is_init ) return

   vtkNumNodes( 1) =  1 !  1: VTK_VERTEX
   vtkNumNodes( 3) =  2 !  3: VTK_LINE
   vtkNumNodes( 5) =  3 !  5: VTK_TRIANGLE
   vtkNumNodes( 8) =  4 !  8: VTK_PIXEL 
   vtkNumNodes( 9) =  4 !  9: VTK_QUAD 
   vtkNumNodes(10) =  4 ! 10: VTK_TETRA 
   vtkNumNodes(11) =  8 ! 11: VTK_VOXEL 
   vtkNumNodes(12) =  8 ! 12: VTK_HEXAHEDRON
   vtkNumNodes(13) =  6 ! 13: VTK_WEDGE
   vtkNumNodes(14) =  5 ! 14: VTK_PYRAMID
   vtkNumNodes(21) =  3 ! 21: VTK_QUADRATIC_EDGE
   vtkNumNodes(22) =  6 ! 22: VTK_QUADRATIC_TRIANGLE
   vtkNumNodes(23) =  8 ! 23: VTK_QUADRATIC_QUAD
   vtkNumNodes(24) = 10 ! 24: VTK_QUADRATIC_TETRA
   vtkNumNodes(25) = 20 ! 20: VTK_QUADRATIC_HEXAHEDRON

   is_init = .true.
      
   END SUBROUTINE conn_vtkNumNodesInit
      
END MODULE conn_m
