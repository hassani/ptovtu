!---------------------------------------------------------------------------------------------
! ptovtu, version 2022.7.0
!---------------------------------------------------------------------------------------------
!
! Module name: 
!       main
!
! Description: 
!       Reads the outputs of a f.e. analysis (in "pfile" format) and writes them in vtu format
! 
! Notes: 
!      - For compatibility reasons, the old adeli pfile format is also taken into account (but
!        not for long !)
!      - The very good VTKFortran library (by S. Zaghi, thanks to him!) is used to encode the
!        vtu files
!
! Author(s): 
!        R. Hassani, Universite Côte d'Azur
!
! Start date: 
!        06/22
!
! Changes:
!        07/22: 
!        08/22:
!        
!---------------------------------------------------------------------------------------------

PROGRAM main

   use ptovtu_m ; use version_m
   
   implicit none
      
   call signalHandler_SignalCatch (unit = STDOUT, title = '--> ptovtu Info:')
!
!- Disable halting after an error is detected in any procedures (callers must check "stat"):
!
   call err_SetHaltingMode ( halting = OFF, unit = STDOUT, DisplayWarning = ON ) 
!
!- Get the version ID:
!
   call version_getVersion ()
!
!- Get the command line arguments:
!   
   call utilptovtu_getCommandArg ()
!
!- Read and parse the user's default file:
!
   call utilptovtu_userDefault ()      
!
!- Proceed:
!      
   call ptovtu ()
!
!- Report errors if any and print an end message:
!
   call utilptovtu_end ()
   
END PROGRAM main
