!---------------------------------------------------------------------------------------------
! ptovtu, version 2022.7.0
!---------------------------------------------------------------------------------------------
!!
! include file "femval_alloc.inc" for the module femval_m. Common lines for subroutines:
!      . femval_allocSp
!      . femval_allocDp
!---------------------------------------------------------------------------------------------

   integer(i64) :: n
   
   if ( present(name) ) then
      self%name = name
   else if (.not. allocated(self%name)) then
      self%name = ''
   end if
   
   if ( present(nvar) ) then
   
      ! if integer members of self are not big enough, resize them:

      if ( allocated(self%varNames)) then
         if (size(self%varNames) < nvar ) &
            deallocate(self%varNames  , self%physNames, self%physUnits, &
                       self%physUnits0, self%varNcomp , self%convUnit , &
                       self%convOper  ,                                 &
                       self%please_convertMe                            )
      end if
      
      if ( .not. allocated(self%varNames) ) &
         allocate(self%varNames  (nvar), self%physNames(nvar), self%physUnits(nvar), &
                  self%physUnits0(nvar), self%varNcomp (nvar), self%convUnit (nvar), &
                  self%convOper  (nvar),                                             &
                  self%please_convertMe(nvar)                                        )
      
      ! save the current number of variables:
      
      self%nvar = nvar
      
   end if

   if ( present(nent) ) self%nent = nent
   
   if ( present(allocv) ) then
   
      if ( allocv ) then
      
         ! compute the size needed to store the nvar variables:
         
         n = sum( self%varNcomp(1:self%nvar) ) * max(1_Ikind,self%nent)
   
         ! if self%v is not big enough, resize it:
         
         if ( allocated(self%v) ) then
            if ( size(self%v) < n) deallocate(self%v)
         end if

         if ( .not. allocated(self%v) ) allocate(self%v(n)) 
   
         ! save the used part of %v (n <= size(self%v)):
         
         self%usedSize = n
         
      end if    
       
   end if
      
