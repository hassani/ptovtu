!---------------------------------------------------------------------------------------------
! ptovtu, version 2022.7.0
!---------------------------------------------------------------------------------------------
!
! include file "femval_writevtu.inc" for the module femval_m. Common lines for subroutines:
!      . femval_writeVtuSp
!      . femval_writeVtuDp
!---------------------------------------------------------------------------------------------

!- local variables: --------------------------------------------------------------------------   
   integer  (Ikind)            :: i, dim(2)
   integer  ( i64 )            :: i1, i2
   integer                     :: err
!--------------------------------------------------------------------------------------------- 

   err = 0
   
   if (.not. allocated(fvtu%xml_writer)) then
      stat = err_t ( stat = IERROR, where = HERE, msg = 'The vtu file is not initialized')  
      return
   end if
   
   if ( open ) then
      err = fvtu%xml_writer%write_dataarray ( location = loc, action = 'open' )
    
      if ( err > 0 ) then
         stat = err_t ( stat = UERROR, where = HERE, msg = 'vtkFortran error (action: "open")' )  
         return
      end if   
   end if
   
   i2 = 0
   do i = 1, self%nvar
      i1 = i2 + 1
      i2 = i2 + self%varNcomp(i) * self%nent
      
      if ( self%please_convertMe(i) ) then
         if ( self%convOper(i) == '*' ) then
            self%v(i1:i2) = self%v(i1:i2) * self%convUnit(i)
         else 
            self%v(i1:i2) = self%v(i1:i2) + self%convUnit(i)
         end if
      end if
      
      dim = [self%varNcomp(i), self%nent]

      if ( allocated(x) ) then
         if ( size(x,1) /= dim(1) .or. size(x,2) /= dim(2) ) deallocate(x)
      end if
      
      if ( .not. allocated(x) ) allocate(x(dim(1),dim(2)))
      
      x = reshape(self%v(i1:i2),dim)

      err = fvtu%xml_writer%write_dataarray                                     &
                           (data_name = trim(self%varNames(i)%str) // '_(' //   &
                                        trim(self%physUnits(i)%str) // ')',     &
                             x        = x                                       )   
                             !x       = reshape(self%v(i1:i2),dim)               )                        
      if ( err > 0 ) exit
   end do

   if ( err > 0 ) then
      stat = err_t ( stat = UERROR, where = HERE, msg = 'vtkFortran error (action: "write")')   
      return
   end if   
   
   if ( close ) then
      err = fvtu%xml_writer%write_dataarray ( location = loc, action = 'close' )   

      if ( err > 0 ) then
         stat = err_t ( stat = UERROR, where = HERE, msg = 'vtkFortran error (action: "close")' )
         return
      end if
   end if  
