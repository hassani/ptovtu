!---------------------------------------------------------------------------------------------
! ptovtu, version 2022.7.0
!---------------------------------------------------------------------------------------------
!
! include file "msh_writeVtu.inc" for the module msh_m. Common lines for subroutines:
!      . msh_writeVtu32Sp
!      . msh_writeVtu32Dp
!      . msh_writeVtu64Sp
!      . msh_writeVtu64Dp
!---------------------------------------------------------------------------------------------

   integer :: err
!
!- Mesh connectivity:
!
   err = fvtu%xml_writer%write_connectivity                            &
                 ( nc           = int(self%ncell,i32),                 &
                   connectivity = self % cellConnect % connect-1,      & ! 0-based
                   offset       = self % cellConnect % indx,           &
                   cell_type    = int(self % cellConnect % types,int8) )

   if ( err > 0 ) then
      stat = err_t ( stat = UERROR, where = HERE, &
                      msg = 'vtkFortran error (action: "write_connectivity")' )
      return
   end if

!
!- Node coordinates:
!
   if ( self%ndime == 3 ) then
      err = fvtu%xml_writer%write_geo ( np  = int(self%npoin,i32), &
                                        nc  = int(self%nCell,i32), &
                                        xyz = self%coord           )  
   else
      ! Unfortunately paraview needs 3 components for coordinates (self%coord) even in cases
      ! where self%ndime < 3. 
      ! Workaround: 
      ! use a local allocatable array (xyz(3,self%npoin)) with the save attribute (xyz must 
      ! be allocated only on the first call to this procedure or when self%npoin has changed)
      ! and copy the first self%ndime components from self%coord to xyz

      if ( allocated(xyz) ) then
         if ( size(xyz,2) /= self%npoin ) then
            deallocate(xyz) ; allocate( xyz(3,self%npoin) )
            xyz(self%ndime+1:3,:) = zero
         end if
      else
         allocate( xyz(3,self%npoin) )
         xyz(self%ndime+1:3,:) = zero
      end if

      xyz(1:self%ndime  ,:) = self%coord
      
      err = fvtu%xml_writer%write_geo ( np  = int(self%npoin,i32), &
                                        nc  = int(self%nCell,i32), &
                                        xyz = xyz                  )        
   end if

   if ( err > 0 ) then
      stat = err_t ( stat = UERROR, where = HERE, &
                      msg = 'vtkFortran error (action = "write_geo")' )
      return
   end if
