!---------------------------------------------------------------------------------------------
! ptovtu, version 2022.7.0
!---------------------------------------------------------------------------------------------
!
! Module name: 
!       ptovtu_m
!
! Description: 
!       Define a DT for a physical quantity, its units and conversions
! 
! Notes: 
!       
!
! Author(s): 
!        R. Hassani, Universite Côte d'Azur
!
! Start date: 
!        06/22
!
! Changes:
!        
!---------------------------------------------------------------------------------------------

#include "error.fpp"

MODULE units_m

   use constantsptovtu_m, only: Ikind, Rkind, IZERO, UERROR
   
   use pk2mod_m,            only: str_t, err_t, err_GetHaltingMode, err_GetWarningMode
   use calmat_m,          only: calmat, G_FREE, G_objs, G_vars, G_nobj
   use dispmodule

   implicit none
   
   private
   public :: phQ_t, units_SetCatalog, units_printCatalog
   
   type :: phQ_t
      logical                       :: is_init = .false.   
      character(len=:), allocatable :: quantityName
      type     (str_t), allocatable :: unitNames(:)
      real     (Rkind), allocatable :: conversionTable(:)
      character(len=1)              :: conversionOperator = '*'      
      integer                       :: choosenUnit = 0
   contains
   end type phQ_t

   interface phQ_t
      module procedure units_SetCatalog
   end interface
   
   
CONTAINS

         
!=============================================================================================
   FUNCTION units_SetCatalog ( filedef, stat ) result ( physQuantCatalog )
!=============================================================================================
   character(len=*), intent(in    ) :: filedef
   type     (err_t), intent(in out) :: stat   
   type     (phQ_t), allocatable    :: physQuantCatalog(:)
!--------------------------------------------------------------------------------------------- 
!  Initializes the set of physical quantities with their some possible units.
!  Reads the user's FDEF file where the units to be used as default are defined
!---------------------------------------------------------------------------------- RH 06/22 - 

!- local variables ---------------------------------------------------------------------------
   character(len=*), parameter   :: HERE = 'units_SetCatalog'
   integer  (Ikind)              :: i, j, k, n, m, var
   character(len=:), allocatable :: str
   type     (str_t), allocatable :: vecS1(:), vecS2(:)
   logical                       :: units, table, operator, choosen
!--------------------------------------------------------------------------------------------- 

!
!- Use calmat to read and parse the default unit file:
! 
   call calmat ( fileIn = filedef, warning = .true.,  &
                 welcome = .false., dispRes = .false., stat = stat )  
   error_TraceNreturn(stat>IZERO, HERE, stat)   
!
!- Retrieve the Physical quantities and their units from the calmat objects:
!
   n = 0
   do i = 1, G_nObj
      if ( G_objs(i)%getStatus() == G_FREE ) cycle
      n = n + 1
   end do
   allocate(physQuantCatalog(n))

   n = 0
   do i = 1, G_nobj
      if ( G_objs(i)%getStatus() == G_FREE ) cycle
      n = n + 1
      
      units = .false. ; table = .false. ; operator = .false. ; choosen = .false.
      do j = 1, G_objs(i)%getCmpNum()
         var = G_objs(i)%getVarId(j)
         str = G_vars(var)%getName()
         k = index(str,'.')
         str = trim(adjustl(str(k+1:)))
         select case ( str )
            case ( 'units' )
               call G_vars(var)%GetMatPacked ( physQuantCatalog(n)%unitNames )
               units = .true.
            case ( 'conversionTable' )
               call G_vars(var)%GetMatPacked ( physQuantCatalog(n)%conversionTable )
               table = .true.
            case ( 'conversionOperator' )
               call G_vars(var)%GetMatPacked ( vecS1 )
               physQuantCatalog(n)%conversionOperator = vecS1(1)%str
               operator = .true.
            case ( 'choosenUnit' ) 
               call G_vars(var)%GetMatPacked ( vecS2 )
               choosen = .true.
         end select
      end do

      if ( units .and. table .and. operator .and. choosen ) then
         physQuantCatalog(n)%is_init = .true.
         physQuantCatalog(n)%quantityName = G_objs(i)%getName()    
      else
         cycle  
      end if
      
      m = size(physQuantCatalog(n)%unitNames)
      if ( m /= size(physQuantCatalog(n)%conversionTable) ) then
         stat = err_t ( msg = 'Arrays "'//physQuantCatalog(n)%quantityName//'.units" ' // &       
                       'and "'//physQuantCatalog(n)%quantityName//'.conversionTable" ' // &
                       'are of different size',stat = UERROR, where = HERE ) 
         return   
      end if
   
      do j = 1, m
         if ( physQuantCatalog(n)%unitNames(j) == vecS2(1) ) then
            physQuantCatalog(n)%choosenUnit = j
            exit
         end if
      end do
   
      if ( physQuantCatalog(n)%choosenUnit == 0 ) then
         stat = err_t ( msg = 'Unexpected choosen unit "'// vecS2(1)%str // '" for ' // &
                       'physical quantity "'// physQuantCatalog(n)%quantityName // '"', &
                        stat = UERROR, where = HERE )    
         return   
      end if
   
   end do

   call units_printCatalog ( physQuantCatalog )
   
   END FUNCTION units_SetCatalog 


!=============================================================================================
   SUBROUTINE units_printCatalog ( physQuantCatalog, title )
!=============================================================================================
   type     (phQ_t),           intent(in) :: physQuantCatalog(:)
   character(len=*), optional, intent(in) :: title
!----------------------------------------------------------------------------------- R.H. 2022

!- local variables ---------------------------------------------------------------------------
   character(len=:), allocatable :: mat(:,:), line, listUnits
   integer                       :: i, j, n, choosen, l1, l2, l3
!---------------------------------------------------------------------------------------------     

   if ( size(physQuantCatalog) == 0 ) return
   
   l1 = len('Quantity') ; l2 = len('Choosen unit') ; l3 =  len('Among  available units'); 

   n = 0
   do i = 1, size(physQuantCatalog)
      if ( .not. physQuantCatalog(i)%is_init ) cycle
      do j = 1, size(physQuantCatalog(i)%unitNames)
         if ( j == 1) then
            listUnits = physQuantCatalog(i)%unitNames(j)%str
         else
            listUnits = listUnits // ', '// physQuantCatalog(i)%unitNames(j)%str
         end  if
      end do
      n = n + 1
      choosen = physQuantCatalog(i)%choosenUnit
      l1 = max(l1,len(physQuantCatalog(i)%quantityName))
      l2 = max(l2,len(physQuantCatalog(i)%unitNames(choosen)%str))     
      l3 = max(l3,len(listUnits))       
   end do
   allocate(character(len=max(l1,l2,l3)) :: mat(n+2,3))
   
   mat(1,1) = 'Quantity' ; mat(1,2) = 'Choosen unit' ; mat(1,3) = 'Among available units'  
   mat(2,1) = repeat('=',l1) ; mat(2,2) = repeat('=',l2) ; mat(2,3) = repeat('=',l3)
    
   n = 0 
   do i = 1, size(physQuantCatalog)
      if ( .not. physQuantCatalog(i)%is_init ) cycle
      do j = 1, size(physQuantCatalog(i)%unitNames)
         if ( j == 1) then
            listUnits = physQuantCatalog(i)%unitNames(j)%str
         else
            listUnits = listUnits // ', '// physQuantCatalog(i)%unitNames(j)%str
         end  if
      end do      
      n = n + 1
      choosen = physQuantCatalog(i)%choosenUnit  
      mat(n+2,1) = physQuantCatalog(i)%quantityName
      mat(n+2,2) = physQuantCatalog(i)%unitNames(choosen)%str
      mat(n+2,3) = listUnits            
   end do

   if ( present(title) ) then
      write(*,'(/,a)') title
   else
      write(*,'(/,a,/,a)') 'Catalog of physical quantities with their ', &
                           'choosen units:'
   end if
   
   line = trim(mat(2,1)) // '===' //  trim(mat(2,2)) // '===' // trim(mat(2,3))
   write(*,'(a)') line       
   call disp (mat, sep = ' | ')
   write(*,'(a,/)') line       

   END SUBROUTINE units_printCatalog
      
END MODULE units_m


      
