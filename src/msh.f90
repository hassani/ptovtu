!---------------------------------------------------------------------------------------------
! ptovtu, version 2022.7.0
!---------------------------------------------------------------------------------------------
!
! Module name: 
!       msh_m
!
! Description: 
!       Define a DT for a general unstructured mesh
! 
! Notes: 
!       Since mesh arrays (connectivity, coordinates, ...) will generally be read from a file, 
!       possibly binary, we define several extended types from an abstract class depending on
!       whether these arrays are int32, int64, real32, real64, ...
!
! Author(s): 
!        R. Hassani, Universite Côte d'Azur
!
! Start date: 
!        06/22
!
! Changes:
!        07/22: 
!        
!---------------------------------------------------------------------------------------------

#include "error.fpp"

MODULE msh_m
   use conn_m,            only: conn32_t, conn64_t, conn_vtkNumNodesInit
   use file_m,            only: file_t 
   use units_m,           only: phQ_t    
   use constantsptovtu_m, only: Ikind, Rkind, i32, i64, rSp, rDp, &
                                NLT, UERROR, WARNING, IZERO, IONE
                                
   use pk2mod_m,            only: err_t, util_IntToChar
   use vtk_fortran,       only: vtk_file  
   
   implicit none
   
   private
   public :: msh_t, msh_init

   ! abstract DT:
   type, abstract :: msh_t
      ! number of cells, boundary facets, boundary edges: 
      integer  (Ikind) :: nCell = 0, nFacet = 0, nEdge = 0
      ! number of nodes and geometrical dimensions:
      integer  (Ikind) :: npoin = 0, ndime = 0      
      ! unit used for nodal coordinates:
      character(len=:), allocatable :: coordUnit, coordUnit0
      real     (Rkind)              :: convUnit = 1.0_Rkind
      logical                       :: please_convertMe = .false.
   contains
      procedure, pass(self) :: InitUnitConversion => msh_InitUnitConversion
      ! deferred methods:  
      procedure(interf_alloc   ), deferred, pass(self) :: alloc
      procedure(interf_connect ), deferred, pass(self) :: readConnect
      procedure(interf_domainId), deferred, pass(self) :: readDomainId
      procedure(interf_domainId), deferred, pass(self) :: readVtkTypeId
      procedure(interf_coord   ), deferred, pass(self) :: readCoord
      procedure(interf_indx    ), deferred, pass(self) :: computeIndx        
      procedure(interf_vtu     ), deferred, pass(self) :: writeVtu  
      
      procedure(interf_setE   ), deferred, pass(self) :: setAnElement  
      procedure(interf_setN   ), deferred, pass(self) :: setANode
      generic :: set =>  setAnElement, setANode
   end type msh_t   

   ! for 32 bits integer and simple precision coordinates:
   type, extends(msh_t) :: m32Sp_t
      ! connectivity tables (for cells, boundary facets, boundary edges):
      type(conn32_t)              :: cellConnect, facetConnect, edgeConnect
      ! nodal coordinates:
      real(rSp     ), allocatable :: coord(:,:)
   contains
      procedure, pass(self) :: alloc          => msh_alloc32Sp  
      procedure, pass(self) :: readConnect    => msh_readConnect32Sp  
      procedure, pass(self) :: readDomainId   => msh_readDomainId32Sp  
      procedure, pass(self) :: readVtkTypeId  => msh_readVtkTypeId32Sp  
      procedure, pass(self) :: readCoord      => msh_readCoord32Sp  
      procedure, pass(self) :: computeIndx    => msh_computeIndx32Sp  
      procedure, pass(self) :: writeVtu       => msh_writeVtu32Sp  
      procedure, pass(self) :: setAnElement   => msh_setAnElement32Sp  
      procedure, pass(self) :: setANode       => msh_setANode32Sp  
   end type m32Sp_t

   ! for 32 bits integer and double precision coordinates:   
   type, extends(msh_t) :: m32Dp_t
      type(conn32_t)              :: cellConnect, facetConnect, edgeConnect   
      real(rDp     ), allocatable :: coord(:,:)
   contains
      procedure, pass(self) :: alloc          => msh_alloc32Dp  
      procedure, pass(self) :: readConnect    => msh_readConnect32Dp 
      procedure, pass(self) :: readDomainId   => msh_readDomainId32Dp  
      procedure, pass(self) :: readVtkTypeId  => msh_readVtkTypeId32Dp  
      procedure, pass(self) :: readCoord      => msh_readCoord32Dp 
      procedure, pass(self) :: computeIndx    => msh_computeIndx32Dp  
      procedure, pass(self) :: writeVtu       => msh_writeVtu32Dp  
      procedure, pass(self) :: setAnElement   => msh_setAnElement32Dp  
      procedure, pass(self) :: setANode       => msh_setANode32Dp  
   end type m32Dp_t

   ! for 64 bits integer and simple precision coordinates:   
   type, extends(msh_t) :: m64Sp_t
      type(conn64_t)              :: cellConnect, facetConnect, edgeConnect
      real(rSp     ), allocatable :: coord(:,:)
   contains
      procedure, pass(self) :: alloc          => msh_alloc64Sp  
      procedure, pass(self) :: readConnect    => msh_readConnect64Sp  
      procedure, pass(self) :: readDomainId   => msh_readDomainId64Sp  
      procedure, pass(self) :: readVtkTypeId  => msh_readVtkTypeId64Sp  
      procedure, pass(self) :: readCoord      => msh_readCoord64Sp  
      procedure, pass(self) :: computeIndx    => msh_computeIndx64Sp  
      procedure, pass(self) :: writeVtu       => msh_writeVtu64Sp 
      procedure, pass(self) :: setAnElement   => msh_setAnElement64Sp  
      procedure, pass(self) :: setANode       => msh_setANode64Sp         
   end type m64Sp_t   
   
   ! for 64 bits integer and double precision coordinates:   
   type, extends(msh_t) :: m64Dp_t
      type(conn64_t)              :: cellConnect, facetConnect, edgeConnect
      real(rDp     ), allocatable :: coord(:,:)
   contains
      procedure, pass(self) :: alloc          => msh_alloc64Dp  
      procedure, pass(self) :: readConnect    => msh_readConnect64Dp
      procedure, pass(self) :: readDomainId   => msh_readDomainId64Dp  
      procedure, pass(self) :: readVtkTypeId  => msh_readVtkTypeId64Dp         
      procedure, pass(self) :: readCoord      => msh_readCoord64Dp  
      procedure, pass(self) :: computeIndx    => msh_computeIndx64Dp  
      procedure, pass(self) :: writeVtu       => msh_writeVtu64Dp  
      procedure, pass(self) :: setAnElement   => msh_setAnElement64Dp  
      procedure, pass(self) :: setANode       => msh_setANode64Dp      
   end type m64Dp_t   

   abstract interface
       subroutine interf_alloc ( self, nCell, nFacet, nEdge, sizeConnect, dimCoord ) 
          import :: msh_t, Ikind, i64
          class    (msh_t),           intent(in out) :: self 
          integer  (Ikind), optional, intent(in    ) :: nCell, nFacet, nEdge, dimCoord(2)
          integer  ( i64 ), optional, intent(in    ) :: sizeConnect 
       end subroutine interf_alloc  

      subroutine interf_coord ( self, file, stat ) 
         import :: msh_t, file_t, err_t
         class  (msh_t ), intent(in out) :: self 
         class  (file_t), intent(in out) :: file 
         type   (err_t ), intent(in out) :: stat
      end subroutine interf_coord   
 
       subroutine interf_connect ( self, file, stat, nCell, nFacet, nEdge ) 
         import :: msh_t, file_t, Ikind, err_t
         class  (msh_t ),           intent(in out) :: self 
         class  (file_t),           intent(in out) :: file 
         type   (err_t ),           intent(in out) :: stat
         integer(Ikind ), optional, intent(in    ) :: nCell, nFacet, nEdge
      end subroutine interf_connect   

       subroutine interf_domainId ( self, length, file, stat, nCell, nFacet, nEdge ) 
         import :: msh_t, file_t, Ikind, err_t
         class  (msh_t ),           intent(in out) :: self 
         integer(Ikind ),           intent(in    ) :: length
         class  (file_t),           intent(in out) :: file 
         type   (err_t ),           intent(in out) :: stat
         integer(Ikind ), optional, intent(in    ) :: nCell, nFacet, nEdge
      end subroutine interf_domainId   
      
       subroutine interf_indx ( self, stat, cells, facets, edges ) 
          import :: msh_t, err_t
          class  (msh_t   ),           intent(in out) :: self 
          type   (err_t   ),           intent(in out) :: stat   
          logical          , optional, intent(in    ) :: cells, facets, edges
       end subroutine interf_indx  
                       
       subroutine interf_vtu ( self, fvtu, stat ) 
          import :: msh_t, vtk_file, err_t
          class    (msh_t   ), intent(in    ) :: self 
          type     (vtk_file), intent(in out) :: fvtu
          type     (err_t   ), intent(in out) :: stat   
       end subroutine interf_vtu    

       subroutine interf_setE ( self, connect, type, domain, elemId, stat ) 
          import :: msh_t, Ikind, err_t
          class    (msh_t), intent(in out) :: self 
          integer  (Ikind), intent(in    ) :: connect(:), type, domain, elemId
          type     (err_t), intent(in out) :: stat   
       end subroutine interf_setE    
       
       subroutine interf_setN ( self, coord, nodeId, stat ) 
          import :: msh_t, Ikind, Rkind, err_t
          class    (msh_t), intent(in out) :: self 
          real     (Rkind), intent(in    ) :: coord(:)
          integer  (Ikind), intent(in    ) :: nodeId
          type     (err_t), intent(in out) :: stat   
       end subroutine interf_setN    

   end interface  

   ! constructor/initializer:
   interface msh_t
      module procedure msh_finit
   end interface
          
CONTAINS

            
!=============================================================================================
   FUNCTION msh_finit ( kindOfInt, kindOfReal, stat ) result ( res )
!=============================================================================================   
   integer(Ikind), intent(in    ) :: kindOfInt, kindOfReal
   type   (err_t), intent(in out) :: stat
   class  (msh_t), allocatable    :: res
!--------------------------------------------------------------------------------------------- 
!  Initializes a msh variables according to the kinds of integer and real.
!---------------------------------------------------------------------------------- RH 06/22 - 
   
   call msh_init ( kindOfInt, kindOfReal, stat, res )
   
   END FUNCTION msh_finit


!=============================================================================================
   SUBROUTINE msh_init ( kindOfInt, kindOfReal, stat, res )
!=============================================================================================   
   integer(Ikind),              intent(in    ) :: kindOfInt, kindOfReal
   type   (err_t),              intent(in out) :: stat
   class  (msh_t), allocatable, intent(   out)    :: res
!--------------------------------------------------------------------------------------------- 
!  Initializes a msh variables according to the kinds of integer and real.
!  Allocates its members with a 0 size
!---------------------------------------------------------------------------------- RH 06/22 - 
   
   if (kindOfInt == 4 .and. kindOfReal == 4) then
      allocate(m32Sp_t :: res)  
   else if (kindOfInt == 4 .and. kindOfReal == 8) then
      allocate(m32Dp_t :: res) 
   else if (kindOfInt == 8 .and. kindOfReal == 4) then
      allocate(m64Sp_t :: res) 
   else if (kindOfInt == 8 .and. kindOfReal == 8) then
      allocate(m64Dp_t :: res) 
   else
      stat = err_t ( stat = UERROR, where = 'msh_init', &
             msg = 'unintended combination of kindOfInt ('//util_intToChar(kindOfInt)//')'//&
                   ' and kindOfReal ('//util_intToChar(kindOfReal)//')' )
      return
   end if
   
   call conn_vtkNumNodesInit ()
   
   END SUBROUTINE msh_init
   
   
!=============================================================================================
   SUBROUTINE msh_InitUnitConversion ( self, catalog, conversion, stat )
!=============================================================================================
   class(msh_t), intent(in out) :: self
   type (phQ_t), intent(in    ) :: catalog(:)
   logical     , intent(in    ) :: conversion
   type (err_t), intent(in out) :: stat   
!--------------------------------------------------------------------------------------------- 
!  Computes the conversion factor for coordinate units according to the user default
!---------------------------------------------------------------------------------- RH 06/22 - 

!- local variables ---------------------------------------------------------------------------
   character(len=*), parameter :: HERE = 'msh_InitUnitConversion'
   integer                     :: i, physqFound, unitFound, choosen
!---------------------------------------------------------------------------------------------  
 
   if ( .not. conversion ) return   
!
!- See if 'coordinate' is referenced in the physical quantities catalog:
!
   physqFound = IZERO
   do i = 1, size(catalog)
      if ( .not. catalog(i)%is_init ) cycle
      if ( catalog(i)%quantityName == 'coordinate' ) then
         physqFound = i ; exit
      end if
   end do

   if (physqFound == IZERO) then
      stat = err_t ( msg='Physical quantity "coordinate" not found in the catalog.'//NLT// &
                         'No conversion will be made for nodal coordinates.',              &
                     stat=WARNING, where=HERE                                              ) 
      self%please_convertMe = .false.
      return
   end if
!
!- See if the unit given for this quantity is present in the list of units used for such 
!  physical quantity in the catalog:
! 
   unitFound = IZERO
   do i = 1, size(catalog(physqFound)%unitNames)
      if ( catalog(physqFound)%unitNames(i) == self%coordUnit ) then
         unitFound = i ; exit
      end if
   end do
 
   if (unitFound == IZERO) then
      stat = err_t ( msg='Unexpected unit "'//self%coordUnit//'" for coordinate.' // NLT //&
                         'No conversion will be made for nodal coordinates.',              &
                     stat=WARNING, where=HERE ) 
      self%please_convertMe = .false.
      return
   end if                              
!
!- The unit # choosen by the user:
!      
   choosen = catalog(physqFound)%choosenUnit
!
!- Store the new unit (save the original one in %physUnits0):
!      
   self%coordUnit0 = self%coordUnit
   self%coordUnit  = catalog(physqFound)%unitNames(choosen)%str
!
!- Compute the conversion factor to switch from the initial unit to the new one:
!      
   if ( choosen == unitFound ) then
      self%please_convertMe = .false.
   else
      self%please_convertMe = .true.      
      self%convUnit = catalog(physqFound)%conversionTable(unitFound) / &
                      catalog(physqFound)%conversionTable(choosen) 
   end if
   
   END SUBROUTINE msh_InitUnitConversion
                           
                                                      
!=============================================================================================
   SUBROUTINE msh_alloc32Sp ( self, nCell, nFacet, nEdge, sizeConnect, dimCoord ) 
!=============================================================================================
   class    (m32Sp_t),           intent(in out) :: self 
   integer  (Ikind  ), optional, intent(in    ) :: nCell, nFacet, nEdge, dimCoord(2)
   integer  ( i64   ), optional, intent(in    ) :: sizeConnect 
!--------------------------------------------------------------------------------------------- 
!  Allocates or reallocates the members of self 
!
!  . if nCell is present: set self%nCell = nCell and allocate %cellTypes, %cellDomains and
!       %cellIndx (or reallocate them only if nCell > size(self%cellTypes)) 
!
!  . if sizeConnect is present: set self%usedSize = sizeConnect and allocate %cellConnect 
!       (or reallocate it only if sizeConnect > size(self%cellConnect))
!
!  . if sizeConnect is present: set self%nDime = dimCoord(1), self%nPoin = dimCoord(2) and
!       allocate %coord (or reallocate it if size(self%coord) /= dimCoord
!---------------------------------------------------------------------------------- RH 06/22 -
#include "inc/msh_alloc.inc"       
   END SUBROUTINE msh_alloc32Sp

!=============================================================================================
   SUBROUTINE msh_alloc32Dp ( self, nCell, nFacet, nEdge, sizeConnect, dimCoord ) 
!=============================================================================================
   class    (m32Dp_t),           intent(in out) :: self 
   integer  (Ikind  ), optional, intent(in    ) :: nCell, nFacet, nEdge, dimCoord(2)
   integer  ( i64   ), optional, intent(in    ) :: sizeConnect 
!---------------------------------------------------------------------------------- RH 06/22 -
#include "inc/msh_alloc.inc"       
   END SUBROUTINE msh_alloc32Dp

!=============================================================================================
   SUBROUTINE msh_alloc64Sp ( self, nCell, nFacet, nEdge, sizeConnect, dimCoord ) 
!=============================================================================================
   class    (m64Sp_t),           intent(in out) :: self 
   integer  (Ikind  ), optional, intent(in    ) :: nCell, nFacet, nEdge, dimCoord(2)
   integer  ( i64   ), optional, intent(in    ) :: sizeConnect 
!---------------------------------------------------------------------------------- RH 06/22 -
#include "inc/msh_alloc.inc"       
   END SUBROUTINE msh_alloc64Sp

!=============================================================================================
   SUBROUTINE msh_alloc64Dp ( self, nCell, nFacet, nEdge, sizeConnect, dimCoord ) 
!=============================================================================================
   class    (m64Dp_t),           intent(in out) :: self 
   integer  (Ikind  ), optional, intent(in    ) :: nCell, nFacet, nEdge, dimCoord(2)
   integer  ( i64   ), optional, intent(in    ) :: sizeConnect 
!---------------------------------------------------------------------------------- RH 06/22 -
#include "inc/msh_alloc.inc"       
   END SUBROUTINE msh_alloc64Dp
         

!=============================================================================================
   SUBROUTINE msh_readCoord32Sp ( self, file, stat )
!=============================================================================================
   class(m32Sp_t), intent(in out) :: self 
   class(file_t ), intent(in out) :: file
   type (err_t  ), intent(in out) :: stat   
!--------------------------------------------------------------------------------------------- 
!  Reads self%coord from a file
!---------------------------------------------------------------------------------- RH 06/22 -

   call file%read ( self%coord, stat, what = 'nodal coordinates' )
   error_TraceNreturn(stat/=IZERO, 'msh_readCoord32Sp', stat)
   if ( self%please_convertMe ) self%coord = self%coord * self%convUnit
                                      
   END SUBROUTINE msh_readCoord32Sp        
   
!=============================================================================================
   SUBROUTINE msh_readCoord32Dp ( self, file, stat )
!=============================================================================================
   class(m32Dp_t), intent(in out) :: self 
   class(file_t ), intent(in out) :: file
   type (err_t  ), intent(in out) :: stat   
!---------------------------------------------------------------------------------- RH 06/22 -
      
   call file%read ( self%coord, stat, what = 'nodal coordinates' )
   error_TraceNreturn(stat/=IZERO, 'msh_readCoord32Dp', stat)
   if ( self%please_convertMe ) self%coord = self%coord * self%convUnit
                                      
   END SUBROUTINE msh_readCoord32Dp     

!=============================================================================================
   SUBROUTINE msh_readCoord64Sp ( self, file, stat )
!=============================================================================================
   class(m64Sp_t), intent(in out) :: self 
   class(file_t ), intent(in out) :: file
   type (err_t  ), intent(in out) :: stat   
!---------------------------------------------------------------------------------- RH 06/22 -
      
   call file%read ( self%coord, stat, what = 'nodal coordinates' )
   error_TraceNreturn(stat/=IZERO, 'msh_readCoord64Sp', stat)
   if ( self%please_convertMe ) self%coord = self%coord * self%convUnit
                                      
   END SUBROUTINE msh_readCoord64Sp        

!=============================================================================================
   SUBROUTINE msh_readCoord64Dp ( self, file, stat )
!=============================================================================================
   class(m64Dp_t), intent(in out) :: self 
   class(file_t ), intent(in out) :: file
   type (err_t  ), intent(in out) :: stat   
!---------------------------------------------------------------------------------- RH 06/22 -
      
   call file%read ( self%coord, stat, what = 'nodal coordinates' )
   error_TraceNreturn(stat/=IZERO, 'msh_readCoord64Dp', stat)
   if ( self%please_convertMe ) self%coord = self%coord * self%convUnit
                                      
   END SUBROUTINE msh_readCoord64Dp     
   
   
!=============================================================================================
   SUBROUTINE msh_readConnect32Sp ( self, file, stat, nCell, nFacet, nEdge )
!=============================================================================================
   class  (m32Sp_t),           intent(in out) :: self 
   class  (file_t ),           intent(in out) :: file
   type   (err_t  ),           intent(in out) :: stat   
   integer(Ikind  ), optional, intent(in    ) :: nCell, nFacet, nEdge
!--------------------------------------------------------------------------------------------- 
!  Reads self%cellConnect, self%cellTypes and self%cellDomains from a file.
!  Then computes %cellIndx
!---------------------------------------------------------------------------------- RH 06/22 -

!- local variables: --------------------------------------------------------------------------  
   character(len=*), parameter :: HERE = 'msh_readConnect32Sp'
!--------------------------------------------------------------------------------------------- 
      
#include "inc/msh_readConnect.inc"       

   END SUBROUTINE msh_readConnect32Sp     

!=============================================================================================
   SUBROUTINE msh_readConnect32Dp ( self, file, stat, nCell, nFacet, nEdge )
!=============================================================================================
   class  (m32Dp_t),           intent(in out) :: self 
   class  (file_t ),           intent(in out) :: file
   type   (err_t  ),           intent(in out) :: stat   
   integer(Ikind  ), optional, intent(in    ) :: nCell, nFacet, nEdge
!---------------------------------------------------------------------------------- RH 06/22 -

!- local variables: --------------------------------------------------------------------------  
   character(len=*), parameter :: HERE = 'msh_readConnect32Dp'
!--------------------------------------------------------------------------------------------- 
      
#include "inc/msh_readConnect.inc"       

   END SUBROUTINE msh_readConnect32Dp     

!=============================================================================================
   SUBROUTINE msh_readConnect64Sp ( self, file, stat, nCell, nFacet, nEdge )
!=============================================================================================
   class  (m64Sp_t),           intent(in out) :: self 
   class  (file_t ),           intent(in out) :: file
   type   (err_t  ),           intent(in out) :: stat   
   integer(Ikind  ), optional, intent(in    ) :: nCell, nFacet, nEdge
!---------------------------------------------------------------------------------- RH 06/22 -

!- local variables: --------------------------------------------------------------------------  
   character(len=*), parameter :: HERE = 'msh_readConnect64Sp'
!--------------------------------------------------------------------------------------------- 
      
#include "inc/msh_readConnect.inc"       

   END SUBROUTINE msh_readConnect64Sp     

!=============================================================================================
   SUBROUTINE msh_readConnect64Dp ( self, file, stat, nCell, nFacet, nEdge )
!=============================================================================================
   class  (m64Dp_t),           intent(in out) :: self 
   class  (file_t ),           intent(in out) :: file
   type   (err_t  ),           intent(in out) :: stat   
   integer(Ikind  ), optional, intent(in    ) :: nCell, nFacet, nEdge
!---------------------------------------------------------------------------------- RH 06/22 -

!- local variables: --------------------------------------------------------------------------  
   character(len=*), parameter :: HERE = 'msh_readConnect64Dp'
!--------------------------------------------------------------------------------------------- 
      
#include "inc/msh_readConnect.inc"       

   END SUBROUTINE msh_readConnect64Dp     

!=============================================================================================
   SUBROUTINE msh_readDomainId32Sp ( self, length, file, stat, nCell, nFacet, nEdge )
!=============================================================================================
   class  (m32Sp_t),           intent(in out) :: self 
   integer(Ikind  ),           intent(in    ) :: length
   class  (file_t ),           intent(in out) :: file
   type   (err_t  ),           intent(in out) :: stat   
   integer(Ikind  ), optional, intent(in    ) :: nCell, nFacet, nEdge
!---------------------------------------------------------------------------------- RH 06/22 -

!- local variables: --------------------------------------------------------------------------  
   character(len=*), parameter :: HERE = 'msh_readDomainId32Sp'
!--------------------------------------------------------------------------------------------- 
      
#include "inc/msh_readDomainId.inc"       

   END SUBROUTINE msh_readDomainId32Sp     

!=============================================================================================
   SUBROUTINE msh_readDomainId32Dp ( self, length, file, stat, nCell, nFacet, nEdge )
!=============================================================================================
   class  (m32Dp_t),           intent(in out) :: self 
   integer(Ikind  ),           intent(in    ) :: length
   class  (file_t ),           intent(in out) :: file
   type   (err_t  ),           intent(in out) :: stat   
   integer(Ikind  ), optional, intent(in    ) :: nCell, nFacet, nEdge
!---------------------------------------------------------------------------------- RH 06/22 -

!- local variables: --------------------------------------------------------------------------  
   character(len=*), parameter :: HERE = 'msh_readDomainId32Dp'
!--------------------------------------------------------------------------------------------- 
      
#include "inc/msh_readDomainId.inc"       

   END SUBROUTINE msh_readDomainId32Dp     
      
!=============================================================================================
   SUBROUTINE msh_readDomainId64Sp ( self, length, file, stat, nCell, nFacet, nEdge )
!=============================================================================================
   class  (m64Sp_t),           intent(in out) :: self 
   integer(Ikind  ),           intent(in    ) :: length
   class  (file_t ),           intent(in out) :: file
   type   (err_t  ),           intent(in out) :: stat   
   integer(Ikind  ), optional, intent(in    ) :: nCell, nFacet, nEdge
!---------------------------------------------------------------------------------- RH 06/22 -

!- local variables: --------------------------------------------------------------------------  
   character(len=*), parameter :: HERE = 'msh_readDomainId64Sp'
!--------------------------------------------------------------------------------------------- 
      
#include "inc/msh_readDomainId.inc"       

   END SUBROUTINE msh_readDomainId64Sp     

!=============================================================================================
   SUBROUTINE msh_readDomainId64Dp ( self, length, file, stat, nCell, nFacet, nEdge )
!=============================================================================================
   class  (m64Dp_t),           intent(in out) :: self 
   integer(Ikind  ),           intent(in    ) :: length
   class  (file_t ),           intent(in out) :: file
   type   (err_t  ),           intent(in out) :: stat   
   integer(Ikind  ), optional, intent(in    ) :: nCell, nFacet, nEdge
!---------------------------------------------------------------------------------- RH 06/22 -

!- local variables: --------------------------------------------------------------------------  
   character(len=*), parameter :: HERE = 'msh_readDomainId64Dp'
!--------------------------------------------------------------------------------------------- 
      
#include "inc/msh_readDomainId.inc"       

   END SUBROUTINE msh_readDomainId64Dp     


!=============================================================================================
   SUBROUTINE msh_readVtkTypeId32Sp ( self, length, file, stat, nCell, nFacet, nEdge )
!=============================================================================================
   class  (m32Sp_t),           intent(in out) :: self 
   integer(Ikind  ),           intent(in    ) :: length
   class  (file_t ),           intent(in out) :: file
   type   (err_t  ),           intent(in out) :: stat   
   integer(Ikind  ), optional, intent(in    ) :: nCell, nFacet, nEdge
!---------------------------------------------------------------------------------- RH 06/22 -

!- local variables: --------------------------------------------------------------------------  
   character(len=*), parameter :: HERE = 'msh_readVtkTypeId32Sp'
!--------------------------------------------------------------------------------------------- 
      
#include "inc/msh_readVtkTypeId.inc"       

   END SUBROUTINE msh_readVtkTypeId32Sp  

!=============================================================================================
   SUBROUTINE msh_readVtkTypeId32Dp ( self, length, file, stat, nCell, nFacet, nEdge )
!=============================================================================================
   class  (m32Dp_t),           intent(in out) :: self 
   integer(Ikind  ),           intent(in    ) :: length
   class  (file_t ),           intent(in out) :: file
   type   (err_t  ),           intent(in out) :: stat   
   integer(Ikind  ), optional, intent(in    ) :: nCell, nFacet, nEdge
!---------------------------------------------------------------------------------- RH 06/22 -

!- local variables: --------------------------------------------------------------------------  
   character(len=*), parameter :: HERE = 'msh_readVtkTypeId32Dp'
!--------------------------------------------------------------------------------------------- 
      
#include "inc/msh_readVtkTypeId.inc"       

   END SUBROUTINE msh_readVtkTypeId32Dp     

!=============================================================================================
   SUBROUTINE msh_readVtkTypeId64Sp ( self, length, file, stat, nCell, nFacet, nEdge )
!=============================================================================================
   class  (m64Sp_t),           intent(in out) :: self 
   integer(Ikind  ),           intent(in    ) :: length
   class  (file_t ),           intent(in out) :: file
   type   (err_t  ),           intent(in out) :: stat   
   integer(Ikind  ), optional, intent(in    ) :: nCell, nFacet, nEdge
!---------------------------------------------------------------------------------- RH 06/22 -

!- local variables: --------------------------------------------------------------------------  
   character(len=*), parameter :: HERE = 'msh_readVtkTypeId64Sp'
!--------------------------------------------------------------------------------------------- 
      
#include "inc/msh_readVtkTypeId.inc"       

   END SUBROUTINE msh_readVtkTypeId64Sp     
   
!=============================================================================================
   SUBROUTINE msh_readVtkTypeId64Dp ( self, length, file, stat, nCell, nFacet, nEdge )
!=============================================================================================
   class  (m64Dp_t),           intent(in out) :: self 
   integer(Ikind  ),           intent(in    ) :: length
   class  (file_t ),           intent(in out) :: file
   type   (err_t  ),           intent(in out) :: stat   
   integer(Ikind  ), optional, intent(in    ) :: nCell, nFacet, nEdge
!---------------------------------------------------------------------------------- RH 06/22 -

!- local variables: --------------------------------------------------------------------------  
   character(len=*), parameter :: HERE = 'msh_readVtkTypeId64Dp'
!--------------------------------------------------------------------------------------------- 
      
#include "inc/msh_readVtkTypeId.inc"       

   END SUBROUTINE msh_readVtkTypeId64Dp     
         

!=============================================================================================
   SUBROUTINE msh_computeIndx32Sp ( self, stat, cells, facets, edges )
!=============================================================================================
   class(m32Sp_t ),           intent(in out) :: self 
   type (err_t   ),           intent(in out) :: stat   
   logical        , optional, intent(in    ) :: cells, facets, edges
!--------------------------------------------------------------------------------------------- 
!
!---------------------------------------------------------------------------------- RH 06/22 - 

   if ( present(cells) ) &
      call self % cellConnect  % computeIndx ( stat, '(Cell-to-Node connectivity)' )
   if ( present(facets)) &
      call self % facetConnect % computeIndx ( stat, '(Facet-to-Node connectivity)')
   if ( present(edges) ) &
      call self % edgeConnect  % computeIndx ( stat, '(Edge-to-Node connectivity)' )
            
   error_TraceNreturn(stat/=IZERO, 'msh_computeIndx32Sp', stat)
   
   END SUBROUTINE msh_computeIndx32Sp 

!=============================================================================================
   SUBROUTINE msh_computeIndx32Dp ( self, stat, cells, facets, edges )
!=============================================================================================
   class(m32Dp_t ),           intent(in out) :: self 
   type (err_t   ),           intent(in out) :: stat   
   logical        , optional, intent(in    ) :: cells, facets, edges
!--------------------------------------------------------------------------------------------- 
!
!---------------------------------------------------------------------------------- RH 06/22 - 

   if ( present(cells) ) &
      call self % cellConnect  % computeIndx ( stat, '(Cell-to-Node connectivity)' )
   if ( present(facets)) &
      call self % facetConnect % computeIndx ( stat, '(Facet-to-Node connectivity)')
   if ( present(edges) ) &
      call self % edgeConnect  % computeIndx ( stat, '(Edge-to-Node connectivity)' )
      
   error_TraceNreturn(stat/=IZERO, 'msh_computeIndx32Dp', stat)
   
   END SUBROUTINE msh_computeIndx32Dp    

!=============================================================================================
   SUBROUTINE msh_computeIndx64Sp ( self, stat, cells, facets, edges )
!=============================================================================================
   class(m64Sp_t ),           intent(in out) :: self 
   type (err_t   ),           intent(in out) :: stat   
   logical        , optional, intent(in    ) :: cells, facets, edges
!--------------------------------------------------------------------------------------------- 
!
!---------------------------------------------------------------------------------- RH 06/22 - 

   if ( present(cells) ) &
      call self % cellConnect  % computeIndx ( stat, '(Cell-to-Node connectivity)' )
   if ( present(facets)) &
      call self % facetConnect % computeIndx ( stat, '(Facet-to-Node connectivity)')
   if ( present(edges) ) &
      call self % edgeConnect  % computeIndx ( stat, '(Edge-to-Node connectivity)' )
            
   error_TraceNreturn(stat/=IZERO, 'msh_computeIndx64Sp', stat)
   
   END SUBROUTINE msh_computeIndx64Sp 

!=============================================================================================
   SUBROUTINE msh_computeIndx64Dp ( self, stat, cells, facets, edges )
!=============================================================================================
   class(m64Dp_t ),           intent(in out) :: self 
   type (err_t   ),           intent(in out) :: stat   
   logical        , optional, intent(in    ) :: cells, facets, edges
!--------------------------------------------------------------------------------------------- 
!
!---------------------------------------------------------------------------------- RH 06/22 - 

   if ( present(cells) ) &
      call self % cellConnect  % computeIndx ( stat, '(Cell-to-Node connectivity)' )
   if ( present(facets)) &
      call self % facetConnect % computeIndx ( stat, '(Facet-to-Node connectivity)')
   if ( present(edges) ) &
      call self % edgeConnect  % computeIndx ( stat, '(Edge-to-Node connectivity)' )
         
   error_TraceNreturn(stat/=IZERO, 'msh_computeIndx64Dp', stat)
   
   END SUBROUTINE msh_computeIndx64Dp       
   
            
!=============================================================================================
   SUBROUTINE msh_writeVtu32Sp ( self, fvtu, stat )
!=============================================================================================
   use, intrinsic :: iso_fortran_env, only: int8
   class(m32Sp_t ), intent(in    ) :: self 
   type (vtk_file), intent(in out) :: fvtu   
   type (err_t   ), intent(in out) :: stat   
!--------------------------------------------------------------------------------------------- 
!  Writes the mesh in the vtu file fvtu (note: fvtu must be initialized)
!  It uses the library VtkFortran of S. Zaghi
!---------------------------------------------------------------------------------- RH 06/22 -
   
!- local variables: --------------------------------------------------------------------------   
   character(len=*), parameter         :: HERE = 'msh_writeVtu32Sp'
   real     ( rSp ), parameter         :: zero = 0.0_rSp
   real     ( rSp ), allocatable, SAVE :: xyz(:,:)
!--------------------------------------------------------------------------------------------- 
#include "inc/msh_writevtu.inc"       
                                             
   END SUBROUTINE msh_writeVtu32Sp 

!=============================================================================================
   SUBROUTINE msh_writeVtu32Dp ( self, fvtu, stat )
!=============================================================================================
   use, intrinsic :: iso_fortran_env, only: int8
   class(m32Dp_t ), intent(in    ) :: self 
   type (vtk_file), intent(in out) :: fvtu   
   type (err_t   ), intent(in out) :: stat   
!---------------------------------------------------------------------------------- RH 06/22 - 
   
!- local variables: --------------------------------------------------------------------------   
   character(len=*), parameter         :: HERE = 'msh_writeVtu32Dp'
   real     ( rSp ), parameter         :: zero = 0.0_rDp
   real     ( rDp ), allocatable, SAVE :: xyz(:,:)
!--------------------------------------------------------------------------------------------- 
#include "inc/msh_writevtu.inc"       
                                             
   END SUBROUTINE msh_writeVtu32Dp 
      
!=============================================================================================
   SUBROUTINE msh_writeVtu64Sp ( self, fvtu, stat )
!=============================================================================================
   use, intrinsic :: iso_fortran_env, only: int8
   class(m64Sp_t ), intent(in    ) :: self 
   type (vtk_file), intent(in out) :: fvtu   
   type (err_t   ), intent(in out) :: stat   
!---------------------------------------------------------------------------------- RH 06/22 - 
   
!- local variables: --------------------------------------------------------------------------   
   character(len=*), parameter         :: HERE = 'msh_writeVtu64Sp'
   real     ( rSp ), parameter         :: zero = 0.0_rSp
   real     ( rSp ), allocatable, SAVE :: xyz(:,:)
!--------------------------------------------------------------------------------------------- 
#include "inc/msh_writevtu.inc"       
                                             
   END SUBROUTINE msh_writeVtu64Sp 

!=============================================================================================
   SUBROUTINE msh_writeVtu64Dp ( self, fvtu, stat )
!=============================================================================================
   use, intrinsic :: iso_fortran_env, only: int8
   class(m64Dp_t ), intent(in    ) :: self 
   type (vtk_file), intent(in out) :: fvtu   
   type (err_t   ), intent(in out) :: stat   
!---------------------------------------------------------------------------------- RH 06/22 - 
   
!- local variables: --------------------------------------------------------------------------   
   character(len=*), parameter         :: HERE = 'msh_writeVtu64Dp'
   real     ( rSp ), parameter         :: zero = 0.0_rDp
   real     ( rDp ), allocatable, SAVE :: xyz(:,:)
!--------------------------------------------------------------------------------------------- 
#include "inc/msh_writevtu.inc"       
                                             
   END SUBROUTINE msh_writeVtu64Dp 
   
         
!=============================================================================================
   SUBROUTINE msh_setAnElement32Sp ( self, connect, type, domain, elemId, stat )
!=============================================================================================   
   class    (m32Sp_t), intent(in out) :: self 
   integer  ( Ikind ), intent(in    ) :: connect(:), type, domain, elemId
   type     ( err_t ), intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  Set the connectivities of the element #elemId
!  Warning: this procedure must be called in ascending order of element numbers
!---------------------------------------------------------------------------------- RH 06/22 - 

!- local variables: --------------------------------------------------------------------------  
   character(len=*), parameter :: HERE = 'msh_setAnElement32Sp'
!--------------------------------------------------------------------------------------------- 
#include "inc/msh_setAnElement.inc"       
   END SUBROUTINE msh_setAnElement32Sp

!=============================================================================================
   SUBROUTINE msh_setAnElement32Dp ( self, connect, type, domain, elemId, stat )
!=============================================================================================   
   class    (m32Dp_t), intent(in out) :: self 
   integer  ( Ikind ), intent(in    ) :: connect(:), type, domain, elemId
   type     ( err_t ), intent(in out) :: stat   
!---------------------------------------------------------------------------------- RH 06/22 - 

!- local variables: --------------------------------------------------------------------------  
   character(len=*), parameter :: HERE = 'msh_setAnElement32Dp'
!--------------------------------------------------------------------------------------------- 
#include "inc/msh_setAnElement.inc"       
   END SUBROUTINE msh_setAnElement32Dp

!=============================================================================================
   SUBROUTINE msh_setAnElement64Sp ( self, connect, type, domain, elemId, stat )
!=============================================================================================   
   class    (m64Sp_t), intent(in out) :: self 
   integer  ( Ikind ), intent(in    ) :: connect(:), type, domain, elemId
   type     ( err_t ), intent(in out) :: stat   
!---------------------------------------------------------------------------------- RH 06/22 - 

!- local variables: --------------------------------------------------------------------------  
   character(len=*), parameter :: HERE = 'msh_setAnElement64Sp'
!--------------------------------------------------------------------------------------------- 
#include "inc/msh_setAnElement.inc"       
   END SUBROUTINE msh_setAnElement64Sp

!=============================================================================================
   SUBROUTINE msh_setAnElement64Dp ( self, connect, type, domain, elemId, stat )
!=============================================================================================   
   class    (m64Dp_t), intent(in out) :: self 
   integer  ( Ikind ), intent(in    ) :: connect(:), type, domain, elemId
   type     ( err_t ), intent(in out) :: stat
!---------------------------------------------------------------------------------- RH 06/22 - 

!- local variables: --------------------------------------------------------------------------  
   character(len=*), parameter :: HERE = 'msh_setAnElement64Dp'
!--------------------------------------------------------------------------------------------- 
#include "inc/msh_setAnElement.inc"       
   END SUBROUTINE msh_setAnElement64Dp


!=============================================================================================
   SUBROUTINE msh_setANode32Sp ( self, coord, nodeId, stat )
!=============================================================================================   
   class    (m32Sp_t), intent(in out) :: self 
   real     ( Rkind ), intent(in    ) :: coord(:)
   integer  ( Ikind ), intent(in    ) :: nodeId
   type     ( err_t ), intent(in out) :: stat   
!--------------------------------------------------------------------------------------------- 
!  Set the coordinates of the node #nodeId
!---------------------------------------------------------------------------------- RH 06/22 - 

!- local variables: --------------------------------------------------------------------------  
   character(len=*), parameter :: HERE = 'msh_setANode32Sp'
!--------------------------------------------------------------------------------------------- 

   if ( size(coord) /= self%nDime ) then
      stat = err_t(stat=UERROR, where=HERE, msg='incompatible size for "coord"')  ; return
   end if
   if ( self%npoin < nodeId ) then
      stat = err_t(stat=UERROR, where=HERE, msg='node Id > number of nodes')  ; return
   end if
   
   if ( self%please_convertMe ) then   
      self%coord(1:self%nDime,nodeId) = coord(:)*self%convUnit
   else
      self%coord(1:self%nDime,nodeId) = coord(:)
   end if
      
   END SUBROUTINE msh_setANode32Sp
   
!=============================================================================================
   SUBROUTINE msh_setANode32Dp ( self, coord, nodeId, stat )
!=============================================================================================   
   class    (m32Dp_t), intent(in out) :: self 
   real     ( Rkind ), intent(in    ) :: coord(:)
   integer  ( Ikind ), intent(in    ) :: nodeId
   type     ( err_t ), intent(in out) :: stat   
!---------------------------------------------------------------------------------- RH 06/22 - 

!- local variables: --------------------------------------------------------------------------  
   character(len=*), parameter :: HERE = 'msh_setANode32Dp'
!--------------------------------------------------------------------------------------------- 

   if ( size(coord) /= self%nDime ) then
      stat = err_t(stat=UERROR, where=HERE, msg='incompatible size for "coord"')  ; return
   end if
   if ( self%npoin < nodeId ) then
      stat = err_t(stat=UERROR, where=HERE, msg='node Id > number of nodes')  ; return
   end if
      
   if ( self%please_convertMe ) then   
      self%coord(1:self%nDime,nodeId) = coord(:)*self%convUnit
   else
      self%coord(1:self%nDime,nodeId) = coord(:)
   end if
      
   END SUBROUTINE msh_setANode32Dp
   
!=============================================================================================
   SUBROUTINE msh_setANode64Sp ( self, coord, nodeId, stat )
!=============================================================================================   
   class    (m64Sp_t), intent(in out) :: self 
   real     ( Rkind ), intent(in    ) :: coord(:)
   integer  ( Ikind ), intent(in    ) :: nodeId
   type     ( err_t ), intent(in out) :: stat   
!---------------------------------------------------------------------------------- RH 06/22 - 

!- local variables: --------------------------------------------------------------------------  
   character(len=*), parameter :: HERE = 'msh_setANode64Sp'
!--------------------------------------------------------------------------------------------- 

   if ( size(coord) /= self%nDime ) then
      stat = err_t(stat=UERROR, where=HERE, msg='incompatible size for "coord"')  ; return
   end if
   if ( self%npoin < nodeId ) then
      stat = err_t(stat=UERROR, where=HERE, msg='node Id > number of nodes')  ; return
   end if
   
   if ( self%please_convertMe ) then   
      self%coord(1:self%nDime,nodeId) = coord(:)*self%convUnit
   else
      self%coord(1:self%nDime,nodeId) = coord(:)
   end if
   
   END SUBROUTINE msh_setANode64Sp

!=============================================================================================
   SUBROUTINE msh_setANode64Dp ( self, coord, nodeId, stat )
!=============================================================================================   
   class    (m64Dp_t), intent(in out) :: self 
   real     ( Rkind ), intent(in    ) :: coord(:)
   integer  ( Ikind ), intent(in    ) :: nodeId
   type     ( err_t ), intent(in out) :: stat   
!---------------------------------------------------------------------------------- RH 06/22 - 

!- local variables: --------------------------------------------------------------------------  
   character(len=*), parameter :: HERE = 'msh_setANode64Dp'
!--------------------------------------------------------------------------------------------- 

   if ( size(coord) /= self%nDime ) then
      stat = err_t(stat=UERROR, where=HERE, msg='incompatible size for "coord"')  ; return
   end if
   if ( self%npoin < nodeId ) then
      stat = err_t(stat=UERROR, where=HERE, msg='node Id > number of nodes')  ; return
   end if
      
   if ( self%please_convertMe ) then   
      self%coord(1:self%nDime,nodeId) = coord(:)*self%convUnit
   else
      self%coord(1:self%nDime,nodeId) = coord(:)
   end if   
   
   END SUBROUTINE msh_setANode64Dp   
   
               
END MODULE msh_m
